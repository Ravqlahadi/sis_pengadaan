<?php
    class m_hps extends CI_Model{

        function __construct() {
            parent::__construct();

        }

        function select() {
            $query  = $this->db->query("SELECT * FROM tabel_hps");
            return $query->result();
        }
        function cek_hps($data){
          $query = $this->db->get_where('tabel_hps',$data);
          return $query;
        }
        function insert($data) {
            $this->db->insert('tabel_hps', $data);
        }
        function insert_detail($data) {
            $this->db->insert('tabel_detail_hps', $data);
        }

        function delete($id) {
            $this->db->delete('tabel_hps', array('id_hps' => $id));
        }

        function update($id_hps,$dat) {

            $this->db->update('tabel_hps', $dat, array('id_hps'=>$id_hps));
        }
        function get_hps_detail($id){
          $query  = $this->db->query("SELECT d.*,b.* FROM tabel_detail_hps d INNER JOIN tabel_barang b ON d.id_barang=b.id_barang WHERE d.id_hps='$id'");
          return $query->result();
        }
        function get($id){
            $this->db->where('id_hps', $id);
            $query = $this->db->get('tabel_hps', 1);
            return $query->result();
        }

    }



?>
