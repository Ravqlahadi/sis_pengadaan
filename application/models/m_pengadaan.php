<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_pengadaan extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }
  public function select(){
    $query = $this->db->query('SELECT p.*,h.nilai_hps FROM tabel_pengadaan p LEFT JOIN tabel_hps h ON p.id_hps=h.id_hps');
    return $query->result();
  }
  public function select_diterima(){
    $query = $this->db->query("SELECT a.*, c.nilai_hps, d.nama_perusahaan, (SELECT COUNT(*) FROM tabel_penawaran b WHERE a.id_pengadaan = b.id_pengadaan) AS peserta FROM tabel_pengadaan a LEFT JOIN tabel_hps c ON c.id_hps=a.id_hps LEFT JOIN tabel_vendor d ON a.id_pemenang=d.id_member WHERE a.status='diterima'");
    return $query->result();
  }
  public function id($id){
    $query = $this->db->query("SELECT id_pengadaan FROM tabel_pengadaan WHERE email_pelamar='$id' ");
    return $query->result();
  }
  public function get($id){
      $query = $this->db->query("SELECT p.*,h.nilai_hps FROM tabel_pengadaan p LEFT JOIN tabel_hps h ON p.id_hps=h.id_hps where p.id_pengadaan='$id'");
    return $query->result();
  }

  public function get_first($id){
    $query = $this->db->get_where('tabel_pengadaan', array('email_pelamar' => $id));
    return $query->result();
  }
  public function get_penawar($id){
    $query = $this->db->query("SELECT a.*,b.nama_perusahaan FROM tabel_penawaran a INNER JOIN tabel_vendor b ON a.id_member=b.id_member WHERE a.id_pengadaan='$id'");
    return $query->result();
  }
  public function get_diterima($id){
    $query = $this->db->query("SELECT a.*, c.nilai_hps, (SELECT COUNT(*) FROM tabel_penawaran b WHERE a.id_pengadaan = b.id_pengadaan) AS peserta FROM tabel_pengadaan a LEFT JOIN tabel_hps c ON c.id_hps=a.id_hps WHERE a.status='diterima' AND a.id_pengadaan='$id'");
    return $query->result();
  }
  public function get_var($id){
    $this->db->select('gaji_pelamar,bidang_pelamar,tgl_lahir_pelamar,jenis_kelamin,status_pelamar,jenjang_pelamar,ipk_pelamar');
    $query = $this->db->get_where('tabel_pengadaan', array('id_pengadaan' => $id));
    return $query->result();
  }
  public function get_akun(){
    $query = $this->db->get_where('tabel_pengadaan', array('akun' => 'false'));
    return $query->result();
  }
  public function count(){
    $query  = $this->db->query("SELECT * FROM tabel_pengadaan");
    return $query->num_rows();
  }
  public function insert_daftar($data) {
    $this->db->insert('tabel_pengadaan', $data);
  }
  public function insert($data){
    $this->db->insert('tabel_pengadaan', $data);
  }

  public function update_akun_status($email,$data){
    $this->db->where('email_pelamar', $email);
    $this->db->update('tabel_pengadaan', $data);
  }
  public function update($data,$id){
    $this->db->where('id_pengadaan', $id);
    $this->db->update('tabel_pengadaan', $data);
  }
  public function update_pengadaan($data,$id){
    $this->db->where('id_pengadaan', $id);
    $this->db->update('tabel_pengadaan', $data);
  }
  public function update_pemenang($id,$data){
    $this->db->where('id_pengadaan', $id);
    $this->db->update('tabel_pengadaan', $data);
  }
  public function delete($id){
    $this->db->delete('tabel_pengadaan', array('id_pengadaan'=>$id));

  }
  public function delete_lamaran($id){
    $query = $this->db->query("DELETE FROM tabel_lamaran WHERE id_lamaran='$id'");
  }
}
