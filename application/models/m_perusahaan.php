<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_perusahaan extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }
  public function register($data){
    $this->db->insert('tabel_vendor', $data);
  }

  public function insert_file_pendukung($data_file) {
	$this->db->update('tabel_vendor', $data_file, array('id_member'=>$data_file['id_member']));
  }

  public function count(){
    $query  = $this->db->query("SELECT * FROM tabel_perusahaan");
    return $query->num_rows();
  }
  public function select(){
    $query  = $this->db->query("SELECT * FROM tabel_perusahaan");
    return $query->result();
  }
  public function get($id){
    $query = $this->db->get_where('tabel_perusahaan', array('id_perusahaan' => $id));
    return $query->result();
  }
  public function id($id){
      $query = $this->db->query("SELECT id_perusahaan FROM tabel_perusahaan WHERE email='$id' ");
    return $query->result();
  }
  public function get_first($id){
    $query = $this->db->get_where('tabel_perusahaan', array('email' => $id));
    return $query->result();
  }
  public function get_akun(){
    $query = $this->db->get_where('tabel_perusahaan', array('akun' => 'false'));
    return $query->result();
  }
  public function insert($data){
    $this->db->insert('tabel_perusahaan', $data);
  }
  public function update($data,$id){
    $this->db->where('id_perusahaan', $id);
    $this->db->update('tabel_perusahaan', $data);
  }
  public function update_akun_status($email,$data){
    $this->db->where('email', $email);
    $this->db->update('tabel_perusahaan', $data);
  }
  public function delete($id){
    $this->db->delete('tabel_perusahaan', array('id_perusahaan'=>$id));
  }
  public function delete_lowongan($id){
    $this->db->delete('tabel_lowongan', array('id_lowongan'=>$id));

  }
}
