<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_lowongan extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }
  public function select(){
    $query = $this->db->query('SELECT a.*, b.nama_job_creator FROM tabel_lowongan a INNER JOIN tabel_job_creator b ON a.id_job_creator=b.id_job_creator');
    return $query->result();
  }
  public function count_lamaran(){
    $query  = $this->db->query("SELECT * FROM tabel_lamaran");
    return $query->num_rows();
  }
  public function count_lowongan(){
    $query  = $this->db->query("SELECT * FROM tabel_lowongan");
    return $query->num_rows();
  }
  public function select_lamaran(){
    $query = $this->db->query("SELECT a.*, b.*, c.id_lamaran, d.nama_job_creator FROM tabel_job_seeker a INNER JOIN tabel_lamaran c ON a.id_job_seeker=c.id_job_seeker LEFT JOIN tabel_lowongan b ON b.id_lowongan=c.id_lowongan INNER JOIN tabel_job_creator d ON d.id_job_creator=b.id_job_creator");
    return $query->result();
  }
  public function lamaran($id){
    $query = $this->db->query("SELECT a.*, b.*, c.id_lamaran, d.nama_job_creator FROM tabel_job_seeker a
      LEFT JOIN tabel_lamaran c ON a.id_job_seeker=c.id_job_seeker
      LEFT JOIN tabel_lowongan b ON b.id_lowongan=c.id_lowongan LEFT JOIN
      tabel_job_creator d ON d.id_job_creator=b.id_job_creator
      WHERE c.id_job_seeker='$id'");
    return $query->result();
  }
  public function lamaran_perusahaan($id){
    $query = $this->db->query("SELECT a.*, b.*, d.nama_job_creator FROM tabel_job_seeker a
      LEFT JOIN tabel_lamaran c ON a.id_job_seeker=c.id_job_seeker
      LEFT JOIN tabel_lowongan b ON b.id_lowongan=c.id_lowongan LEFT JOIN
      tabel_job_creator d ON d.id_job_creator=b.id_job_creator
      WHERE d.id_job_creator='$id'");
    return $query->result();
  }
  public function get($id){
    $query = $this->db->get_where('tabel_lowongan', array('id_job_creator' => $id));
    return $query->result();
  }
  public function get_lowongan($id){
    $query = $this->db->get_where('tabel_lowongan', array('id_lowongan' => $id));
    return $query->result();
  }
  public function get_pelamar($id){
    $query = $this->db->query("SELECT tabel_job_seeker.* FROM tabel_job_seeker
      INNER JOIN tabel_lamaran ON tabel_lamaran.id_job_seeker=tabel_job_seeker.id_job_seeker
       WHERE tabel_lamaran.id_lowongan='$id'");
    return $query->result();
  }

  public function insert($data){
    $query = $this->db->insert('tabel_lowongan', $data);
  }
  public function submit($data){
    $query = $this->db->insert('tabel_lamaran', $data);
  }

  public function update($data){
    $this->db->where('id_lowongan', $data['id_lowongan']);
    $this->db->update('tabel_lowongan', $data);
  }
  public function edit_lamaran($data){
    $this->db->where('id_lamaran', $data['id_lamaran']);
    $this->db->update('tabel_lamaran', $data);
  }
  public function delete($id){
    $this->db->delete('tabel_lowongan', array('id_lowongan'=>$id));

  }
  public function delete_lamaran($id){
    $this->db->delete('tabel_lamaran', array('id_lamaran'=>$id));

  }

}
