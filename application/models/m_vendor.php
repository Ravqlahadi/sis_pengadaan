<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_vendor extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }
  public function register($data){
    $this->db->insert('tabel_vendor', $data);
  }

  public function count(){
    $query  = $this->db->query("SELECT * FROM tabel_vendor");
    return $query->num_rows();
  }
  public function select(){
    $query  = $this->db->query("SELECT * FROM tabel_vendor");
    return $query->result();
  }
  public function select_diikuti(){
    $query  = $this->db->query("SELECT * FROM tabel_vendor");
    return $query->result();
  }

  public function get($id){
    $query = $this->db->get_where('tabel_vendor', array('id_vendor' => $id));
    return $query->result();
  }
  public function get_email($id){
    $query = $this->db->get_where('tabel_vendor', array('email' => $id));
    return $query->result();
  }
  public function get_diikuti($id){
    $query = $this->db->query("SELECT p.*, pd.* FROM tabel_penawaran p INNER JOIN tabel_pengadaan pd ON p.id_pengadaan=pd.id_pengadaan WHERE p.id_member='$id'");
    return $query->result();
  }


  public function get_member($id){
    $query = $this->db->get_where('tabel_vendor', array('id_member' => $id));
    return $query->result();
  }
  public function id($id){
      $query = $this->db->query("SELECT id_vendor FROM tabel_vendor WHERE email='$id' ");
    return $query->result();
  }
  public function get_first($id){
    $query = $this->db->get_where('tabel_vendor', array('email' => $id));
    return $query->result();
  }
  public function get_akun(){
    $query = $this->db->get_where('tabel_vendor', array('akun' => 'false'));
    return $query->result();
  }
  public function insert($data){
    $this->db->insert('tabel_vendor', $data);
  }
  public function insert_penawaran($data){
    $this->db->insert('tabel_penawaran', $data);
  }
  public function update($data,$id){
    $this->db->where('id_vendor', $id);
    $this->db->update('tabel_vendor', $data);
  }
  public function update_akun_status($email,$data){
    $this->db->where('email', $email);
    $this->db->update('tabel_vendor', $data);
  }
  public function delete($id){
    $this->db->delete('tabel_vendor', array('id_vendor'=>$id));
  }
  public function delete_lowongan($id){
    $this->db->delete('tabel_lowongan', array('id_lowongan'=>$id));

  }
}
