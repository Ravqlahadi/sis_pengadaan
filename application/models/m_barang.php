<?php
    class m_barang extends CI_Model{

        function __construct() {
            parent::__construct();

        }

        function select() {
            $query  = $this->db->query("SELECT * FROM tabel_barang");
            return $query->result();
        }
        function cek_barang($data){
          $query = $this->db->get_where('tabel_barang',$data);
          return $query;
        }
        function insert($data) {
            $this->db->insert('tabel_barang', $data);
        }

        function delete($id) {
            $this->db->delete('tabel_barang', array('id_barang' => $id));
        }

        function update($data) {
            $this->user_name = $data['user_name'];
            if($data['user_password']!=""){
              $this->user_password = $data['user_password'];
            }
            $this->level = $data['level'];
            $this->db->update('tabel_barang', $this, array('id_barang'=>$data['id_barang']));
        }
        function get_barang_detail($id){
            $this->db->where('id_barang', $id);
            $query = $this->db->get('tabel_detail_barang');
            return $query->result();
        }
        function get($id){
            $this->db->where('id_barang', $id);
            $query = $this->db->get('tabel_barang', 1);
            return $query->result();
        }

    }



?>
