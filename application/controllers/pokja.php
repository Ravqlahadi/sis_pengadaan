<?php  if ( ! defined('BASEPATH')) exit('no direct script access allowed');
session_start();
class pokja extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$level=$this->session->userdata('level');
		if ($level!="pokja") {
			if($level=='vendor'){
				redirect('vendor/beranda');
			}elseif ($level=='ppk') {
				redirect('ppk/beranda');
			}else{
					redirect('auth');
			}
		}
	}

	public function index() {
		$this->load->model(array('m_pengadaan','m_vendor'));
		$data['daftar_pengadaan']=$this->m_pengadaan->select_diterima();
		$data['menu']='beranda';
		$this->load->view('pokja/index', $data);
	}
	public function daftar_paket() {
		$this->load->model(array('m_pengadaan'));
		$data['daftar_pengadaan']=$this->m_pengadaan->select();
		$data['menu']='daftar_paket';
		$this->load->view('pokja/index', $data);
	}
	public function detail_pengadaan(){
    $this->load->model(array('m_hps','m_pengadaan','m_barang'));
    $id_pengadaan = $this->input->get('id');
    $data["data_pengadaan"] = $this->m_pengadaan->get_diterima($id_pengadaan);
		$data["data_penawar"] = $this->m_pengadaan->get_penawar($id_pengadaan);
    $data['menu']='detail_pengadaan';
    $this->load->view('pokja/index', $data);
  }
	public function pemenang(){
		$this->load->model(array('m_pengadaan'));
		$id=$this->input->get('id_pengadaan');
		$data['id_pemenang']=$this->input->get('id_member');
		$this->m_pengadaan->update_pemenang($id,$data);
		redirect('pokja');

	}
	public function detail_hps(){
		$this->load->model(array('m_hps','m_pengadaan','m_barang'));
		$id_pengadaan = $this->input->get('id');
		$data_pengadaan = $this->m_pengadaan->get($id_pengadaan);
		foreach ($data_pengadaan as $k) {
			if ($k->id_hps=="") {
				$dat['id_hps']='hps_'.$id_pengadaan;
				$this->m_hps->insert($dat);
				$this->m_pengadaan->update($dat,$id_pengadaan);
				//redirect('pokja/detail_hps');
				$data['daftar_hps']=$this->m_hps->get_hps_detail($dat['id_hps']);
			}else {
				$id_hps=$k->id_hps;
				$data['daftar_hps']=$this->m_hps->get_hps_detail($id_hps);
			}

		}
		$data['daftar_barang']=$this->m_barang->select();

		$data['menu']='hps';
		$this->load->view('pokja/index', $data);
	}

	public function bukti_pembayaran(){
		$this->load->model(array('m_hps','m_pengadaan','m_barang'));
		$id_pengadaan = $this->input->get('id');
		$data["data_pengadaan"] = $this->m_pengadaan->get($id_pengadaan);
		$data['menu']='bukti_pembayaran';
		$this->load->view('pokja/index', $data);
	}

	public function daftar_vendor() {
		$this->load->model(array('m_vendor'));

		$id_vendor = $this->input->post('id_vendor');
		if ($id_vendor!="") {
			$data['data_perusahaan']=$this->m_vendor->get($id_vendor);

		}else{
			$data['data_perusahaan']="";
		}
		//var_dump($data['data_perusahaan']);
		$data['daftar_vendor']=$this->m_vendor->select();
		$data['menu']='daftar_vendor';
		$this->load->view('pokja/index', $data);
	}
	public function daftar_barang() {
		$this->load->model(array('m_barang'));
		$data['daftar_barang']=$this->m_barang->select();
		$data['menu']='daftar_barang';
		$this->load->view('pokja/index', $data);
	}

	public function input_barang(){
		$this->load->model(array('m_barang'));
		$data['id_barang']=$this->input->post('id_barang');
		$data['nama_barang']=$this->input->post('nama_barang');
		$data['satuan_barang']=$this->input->post('satuan_barang');
		$data['harga_barang']=$this->input->post('harga_barang');


		$this->m_barang->insert($data);
		redirect('pokja/daftar_barang');
	}
	public function input_hps(){
		$this->load->model(array('m_hps','m_barang'));
		$data['id_hps'] = "hps_".$this->input->post('id_hps');
		$data_hps = $this->m_hps->get($data['id_hps']);
		$nilai_hps=$data_hps[0]->nilai_hps;

		$data['id_barang'] = $this->input->post('id_barang');
		$data_barang = $this->m_barang->get($data['id_barang']);
		$harga_barang = $data_barang[0]->harga_barang;
		$data['volume_barang'] = $this->input->post('volume_barang');
		$data['total_harga'] = $harga_barang * $data['volume_barang'];
		$id_hps= $data['id_hps'];
		$ppn = ($data['total_harga']*10)/100;
		$dat['nilai_hps'] = $nilai_hps+($data['total_harga']+$ppn);

		var_dump($dat['nilai_hps']);
		$this->m_hps->insert_detail($data);
		$this->m_hps->update($id_hps,$dat);
		redirect('pokja/detail_hps?id='.$this->input->post('id_hps').'');
	}

	public function input_pengadaan(){
		$this->load->model(array('m_pengadaan'));
		$data['id_pengadaan']=$this->input->post('id_pengadaan');
		$data['nama_pengadaan']=$this->input->post('nama_pengadaan');
		$data['jenis_pengadaan']=$this->input->post('jenis_pengadaan');
		$data['lokasi_pekerjaan']=$this->input->post('lokasi_pengadaan');
		$mulai = $this->input->post('jadwal_mulai');
		$selesai = $this->input->post('jadwal_selesai');
		$jadwal_pengadaan = "Mulai ".$mulai." Sampai Dengan ".$selesai;
		$data['jadwal_pengadaan']=$jadwal_pengadaan;
		$data['dokumen']=$this->input->post('dokumen');
		$data['id_pengadaan']=$this->input->post('id_pengadaan');
		$data['status']="diperiksa";

		$config['upload_path'] = './upload/dokumen/';
		$config['allowed_types'] = "doc|docx|pdf|png";
		$config['overwrite']="true";
		$config['max_size']="20000000";
		$config['max_width']="10000";
		$config['max_height']="10000";
		$config['file_name'] = 'dokumen_'.$data['nama_pengadaan'];
		$this->upload->initialize($config);

		if(!$this->upload->do_upload()){
			echo  $this->upload->display_errors();

		}else {

				$dat = $this->upload->data();
				$data['dokumen'] = $dat['file_name'];
				$this->m_pengadaan->insert($data);
				redirect('pokja/daftar_paket');
		}


	}


	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect(site_url());
	}


}
?>
