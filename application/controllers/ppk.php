<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ppk extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    $level=$this->session->userdata('level');
    if($level!='ppk' || $level==""){
      redirect('auth');
    }
  }

  function index()
  {
    $this->beranda();
  }



  public function beranda(){
    $this->load->model(array('m_pengadaan','m_vendor'));
		$data['daftar_pengadaan']=$this->m_pengadaan->select_diterima();
		$data['menu']='beranda';
    $this->load->view('ppk/index',$data);
  }
  public function daftar_paket(){
    $this->load->model(array('m_pengadaan'));
		$data['daftar_pengadaan']=$this->m_pengadaan->select();
    $data['menu']='daftar_paket';
    $this->load->view('ppk/index',$data);
  }

  public function detail_hps(){
    $this->load->model(array('m_hps','m_pengadaan','m_barang'));
    $id_pengadaan = $this->input->get('id');
    $data_pengadaan = $this->m_pengadaan->get($id_pengadaan);
    foreach ($data_pengadaan as $k) {
      if ($k->id_hps=="") {
        $dat['id_hps']='hps_'.$id_pengadaan;
        $this->m_hps->insert($dat);
        $this->m_pengadaan->update($dat,$id_pengadaan);
        redirect('ppk/detail_hps');
      }else {
        $id_hps=$k->id_hps;
      }

    }
    $data['daftar_barang']=$this->m_barang->select();
    $data['daftar_hps']=$this->m_hps->get_hps_detail($id_hps);

    $data['menu']='hps';
    $this->load->view('ppk/index', $data);
  }


    public function detail_pengadaan(){
      $this->load->model(array('m_hps','m_pengadaan','m_barang'));
      $id_pengadaan = $this->input->get('id');
      $data["data_pengadaan"] = $this->m_pengadaan->get($id_pengadaan);
      $data['menu']='detail_pengadaan';
      $this->load->view('ppk/index', $data);
    }
    public function spmk(){
      $this->load->model(array('m_hps','m_pengadaan','m_barang'));
      $id_pengadaan = $this->input->get('id');
      $data["data_pengadaan"] = $this->m_pengadaan->get($id_pengadaan);
      $data['menu']='spmk';
      $this->load->view('ppk/index', $data);
    }

    public function sp(){
      $this->load->model(array('m_hps','m_pengadaan','m_barang'));
      $id_pengadaan = $this->input->get('id');
      $data["data_pengadaan"] = $this->m_pengadaan->get($id_pengadaan);
      $data['menu']='sp';
      $this->load->view('ppk/index', $data);
    }

    public function bstp(){
      $this->load->model(array('m_hps','m_pengadaan','m_barang'));
      $id_pengadaan = $this->input->get('id');
      $data["data_pengadaan"] = $this->m_pengadaan->get($id_pengadaan);
      $data['menu']='bstp';
      $this->load->view('ppk/index', $data);
    }


  public function daftar_vendor() {
		$this->load->model(array('m_vendor'));

		$id_vendor = $this->input->post('id_vendor');
		if ($id_vendor!="") {
			$data['data_perusahaan']=$this->m_vendor->get($id_vendor);

		}else{
			$data['data_perusahaan']="";
		}
		//var_dump($data['data_perusahaan']);
		$data['daftar_vendor']=$this->m_vendor->select();
		$data['menu']='daftar_vendor';
		$this->load->view('ppk/index', $data);
	}

  public function terima_pengadaan(){
    $this->load->model(array('m_pengadaan'));
    $id = $this->input->get('id');
    $data['status']='diterima';
    $this->m_pengadaan->update_pengadaan($data,$id);
    //var_dump($id);
    redirect('ppk/beranda');
  }

  public function input_spmk(){
    require_once('./assets/html2pdf/html2pdf.class.php');
    $this->load->view('ppk/report/spmk');
  }
  public function input_sp(){
    require_once('./assets/html2pdf/html2pdf.class.php');
    $this->load->view('ppk/report/sp');
  }
  public function input_btsp(){
    require_once('./assets/html2pdf/html2pdf.class.php');
    $this->load->view('ppk/report/btsp');
  }

  public function logout() {
    $this->session->unset_userdata('username');
    $this->session->unset_userdata('level');
    session_destroy();
    redirect(site_url());
  }


}
