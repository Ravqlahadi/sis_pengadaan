<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class vendor extends CI_Controller{

  public function __construct()
  {
    parent::__construct();


    $level=$this->session->userdata('level');
    if($level!='vendor' || $level == ""){
      redirect('auth');
    }
    //Codeigniter : Write Less Do More
  }

  function index()
  {
    redirect('vendor/beranda');
  }

  //navigasi menu

  public function beranda(){
    $this->load->model(array('m_vendor'));
    $email_vendor =  $this->session->userdata('username')."@gmail.com";
    $daftar_vendor=$this->m_vendor->get_email($email_vendor);


    $id_member = $daftar_vendor[0]->id_member;
    $data['daftar_pengadaan']=$this->m_vendor->get_diikuti($id_member);
    //var_dump($data['daftar_pengadaan']);
    $data['menu']='beranda';
    $this->load->view('vendor/index',$data);
  }
  public function pengadaan(){
    $this->load->model(array('m_pengadaan'));
    $data['daftar_pengadaan']=$this->m_pengadaan->select_diterima();
    $data['menu']='pengadaan';
    $this->load->view('vendor/index',$data);
  }
  public function penawaran_harga(){
    $this->load->model(array('m_vendor'));

    $email_vendor =  $this->session->userdata('username')."@gmail.com";
    $daftar_vendor=$this->m_vendor->get_email($email_vendor);
    $id_member = $daftar_vendor[0]->id_member;

    $data['id_pengadaan'] = $this->input->post('id_pengadaan');
    $data['id_member'] = $id_member;
    //$data['dokumen_kualifikasi'] = $this->input->post('dokumen_kualifikasi');
    $data['nilai_penawaran'] = $this->input->post('nilai_penawaran');
    $config['upload_path'] = './upload/dokumen/';
    $config['allowed_types'] = "doc|docx|pdf|png";
    $config['overwrite']="true";
    $config['max_size']="20000000";
    $config['max_width']="10000";
    $config['max_height']="10000";
    $config['file_name'] = 'dokumen_penawaran'.$data['id_pengadaan'].$data['id_member'];
    $this->upload->initialize($config);

    if(!$this->upload->do_upload()){
      echo  $this->upload->display_errors();

    }else {

        $dat = $this->upload->data();
        $data['dokumen_kualifikasi'] = $dat['file_name'];
        $this->m_vendor->insert_penawaran($data);
        redirect('vendor/beranda');
    }



  }

  public function detail_hps(){
    $this->load->model(array('m_hps','m_pengadaan','m_barang'));
    $id_pengadaan = $this->input->get('id');

    $data_pengadaan = $this->m_pengadaan->get($id_pengadaan);

    foreach ($data_pengadaan as $k) {
      if ($k->id_hps=="") {
        $dat['id_hps']='hps_'.$id_pengadaan;
        $this->m_hps->insert($dat);
        $this->m_pengadaan->update($dat,$id_pengadaan);
        redirect('vendor/detail_hps');
      }else {
        $id_hps=$k->id_hps;
      }

    }

    $data['daftar_barang']=$this->m_barang->select();
    $data['daftar_hps']=$this->m_hps->get_hps_detail($id_hps);

    $data['menu']='detail_hps';
    $this->load->view('vendor/index', $data);
  }

  public function detail_pengadaan(){
    $this->load->model(array('m_hps','m_pengadaan','m_barang'));
    $id_pengadaan = $this->input->get('id');
    $data["data_pengadaan"] = $this->m_pengadaan->get($id_pengadaan);
    $data['menu']='detail_pengadaan';
    $this->load->view('vendor/index', $data);
  }


  public function daftar_vendor(){
    $this->load->model(array('m_vendor'));
    $id_member =$this->session->userdata('id_member');
    //var_dump($id_member);
		$data['data_perusahaan']=$this->m_vendor->get_member($id_member);

		//var_dump($data['data_perusahaan']);
		$data['daftar_vendor']=$this->m_vendor->select();
    $data['menu']='vendor';
    $this->load->view('vendor/index',$data);
  }

  public function logout() {
    $this->session->unset_userdata('username');
    $this->session->unset_userdata('level');
    session_destroy();
    redirect(site_url());
  }
}
