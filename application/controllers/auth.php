<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function index() {
		$this->load->view('landing-page');

	}


	public function cek_login() {
		$data = array('username' => $this->input->post('username', TRUE),
						'password' => md5($this->input->post('password', TRUE))
			);
		$this->load->model(array('m_user')); // load model_user

		$hasil = $this->m_user->cek_user($data);
		$set_data = $this->m_user->cek_member($data);
		if ($set_data->num_rows() == 1) {
			foreach ($set_data->result() as $s) {
				$sess_data['id_member'] = $s->id_member;
				$this->session->set_userdata($sess_data);
			}
		}

		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Loggin';
				$sess_data['email'] = $sess->username;
				$sess_data['username'] = $sess->nama_user;
				$sess_data['level'] = $sess->level;
				$this->session->set_userdata($sess_data);
			}
			if ($this->session->userdata('level')=='pokja') {
				redirect('pokja');
			}elseif ($this->session->userdata('level')=='vendor') {
				redirect('vendor/beranda');
			}elseif ($this->session->userdata('level')=='ppk') {
				redirect('ppk/beranda');
			};
		}
		else {
				echo "<script>alert('Gagal login: Cek username, password!');</script>";
				redirect('home/login');
		}

	}
	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect(site_url());
	}

}

?>
