<?php if ( ! defined('BASEPATH')) exit('no direct script access allowed');
Class home extends CI_Controller {
      function __construct(){
        parent::__construct();
        //$this->load->model(m_login);
        if ($this->session->userdata('level')=='pokja') {
  				redirect('pokja/beranda');
  			}elseif ($this->session->userdata('level')=='vendor') {
  				redirect('vendor/beranda');
  			}elseif ($this->session->userdata('level')=='ppk') {
  				redirect('ppk/beranda');
  			}else{

        };
      }
  public function index(){
    $this->load->model(array('m_pengadaan'));
		$data['daftar_pengadaan']=$this->m_pengadaan->select_diterima();
    $data['result']="";
    $this->load->view('landing-page',$data);

  }
  public function pilih_register(){
    $this->load->view('pilih_register');
  }
  public function login(){
    $data['user'] = $this->input->get('user');

    $this->load->view('login-page',$data);

  }

  public function register_vendor(){
    $this->load->view('register_vendor');
  }


  public function daftar(){
    $this->load->view('register_page');
  }
  public function form_daftar(){
    $this->load->model(array('m_user','m_perusahaan'));

  //Input ke Tabel User
  $data_user['username']=$this->input->post('username');
  $data_user['password']=MD5($this->input->post('password'));
  $data_user['nama_user']=$this->input->post('username');
  $data_user['level']='vendor';
  $this->m_user->insert($data_user);

  //Input ke Tabel Member
  $data_member['id_member']="MBR-".date('YmdHis');
  $data_member['username']=$this->input->post('username');
  $data_member['password']=MD5($this->input->post('password'));
  $data_member['email']=$this->input->post('email');
  $this->m_user->insert_member($data_member);


  //Input ke Tabel Vendor
  $data_vendor['id_member']=$data_member['id_member'];
  $data_vendor['nama_perusahaan']=$this->input->post('nama');
  $data_vendor['alamat']=$this->input->post('alamat');
  $data_vendor['email']=$this->input->post('email');
  $data_vendor['no_telpon']=$this->input->post('nomor_telepon');
  $data_vendor['npwp']=$this->input->post('npwp');
  $data_vendor['izin_usaha']=$this->input->post('izin_usaha');
  $data_vendor['no_izin_usaha']=$this->input->post('nomor_izin_usaha');
  $data_vendor['no_akta_perusahaan']=$this->input->post('no_akta');
  $data_vendor['tanggal']=$this->input->post('tanggal_akta');
  $data_vendor['nama_notaris']=$this->input->post('nama_notaris');
  $this->m_perusahaan->register($data_vendor);


  //Input File Pendukung Perusahaan
  for($i=0;$i<4;$i++){
    $file=$_FILES['userfile']['name'];
    if($file[$i]==""){

    }else{
      $colum=$this->input->post('namaKolom');
      $nama_file[$i]=$_FILES['userfile']['name'][$i];
      $kode=$data_vendor['nama_perusahaan'];
      $all=$kode."-".$nama_file[$i];//generate nama file
      $lokasi_awal[$i]=$_FILES['userfile']['tmp_name'][$i];
      $path='./upload/'.$colum[$i].'/';
      move_uploaded_file($lokasi_awal[$i],$path.$all);
      $data_file['id_member'] = $data_member['id_member'];
      $data_file[$colum[$i]] = $all;
      $this->m_perusahaan->insert_file_pendukung($data_file);

    }
  }

  redirect('home/login?user=penyedia');

  }






}

?>
