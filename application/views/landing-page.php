
<!DOCTYPE html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Dinas PU | Kabupaten Konawe Utara</title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg top-navigation">

  <div id="wrapper">
          <div id="" class="gray-bg">
            <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-md-1 col-md-offset-1" style="text-align:right">
              <img src="<?php echo base_url()?>/assets/img/logo-konut.png" style="height:100px;" alt="" />
            </div>
            <div class="col-md-8"  style="text-align:center;margin-top:10px" >
              <h1>Layanan Pengadaan Barang dan Jasa Pemerintah</h1> <h1 style="margin-top:-5px">Dinas Pekerjaan Umum Kabupaten Konawe Utara</h1>
            </div>
            <div class="col-md-1" style="padding-top:10px;text-align:left" >
              <img src="<?php echo base_url()?>/assets/img/logo-pu.png" style="height:100px;" alt="" />
            </div>
        </div>
          <div class="row border-bottom white-bg">
          <nav class="navbar navbar-static-top" role="navigation">
            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                <i class="fa fa-reorder"></i>
            </button>
              <div class="navbar-collapse collapse" id="navbar">
                <div class="row">
                  <div class="col-md-8 col-md-offset-1">
                    <ul class="nav navbar-nav">
                      <li>
                          <a href="<?php echo site_url()?>">
                              <i class="fa fa-home"></i> Beranda
                          </a>
                      </li>
                      <li>
                          <a href="#">
                              <i class="fa fa-newspaper-o"></i> Tentang Kami
                          </a>
                      </li>

                    </ul>
                  </div>
                </div>


              </div>
          </nav>
          </div>
          <div class="wrapper wrapper-content">
              <div class="container">
                <div class="row">
                  <div class="col-lg-8">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Pengumuman Pengadaan</h5>
                          </div>
                        <div class="ibox-content">
                          <div class='row '>
                            <div class="col-md-12">
                              <div class="table-responsive">
                                  <table class="table" >
                                  <!--  <thead>
                                    <tr>

                                        <th>Nama Pengadaan</th>
                                        <th>Jenis Pengadaan</th>
                                        <th>Jadwal</th>
                                        <th>Lokasi</th>
                                        <th>HPS</th>
                                        <th>Dokumen</th>

                                    </tr>
                                  </thead>-->
                                    <tbody>
                                      <?php
                                        $no=1;
                                        foreach ($daftar_pengadaan as $k) { ?>
                                          <tr>

                                            <td style="width:10px;"><?php echo $no;?>.</td>
                                            <td><a href="<?php echo base_url('upload/dokumen')."/".$k->dokumen?>"><?php echo $k->nama_pengadaan;?></a></td>
                                          </tr>
                                        <?php $no++; } ?>
                                    </tbody>

                                  </table>
                                </div>
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>

                  <div class="col-lg-4">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Silahkan Masuk!<small> Untuk Melanjutkan</small></h5>
                          </div>
                        <div class="ibox-content">
                            <div class="text-center">
                              <div class="row" style="padding-top:20px;padding-bottom:20px">
                                <div class="col-md-12">
                                  <a href="<?php echo site_url('home/login')?>?user=panitia" class="btn btn-primary" style="width:100%">Login Panitia</a>
                                </div>
                                <div class="col-md-12">
                                  <a href="<?php echo site_url('home/login')?>?user=penyedia" class="btn btn-warning" style="width:100%;margin-top:10px">Login Penyedia</a>
                                </div>
                              </div>

                            </div>

                          </div>
                      </div>
                  </div>
                </div>
              </div>
          </div>


          </div>
          </div>



    <!-- Mainly scripts -->
    <script src="<?php echo base_url()?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url()?>assets/js/plugins/pace/pace.min.js"></script>
    <script type="text/javascript">
          $('#myModal').on('shown.bs.modal', function () {
      $('#myInput').focus()
      })
    </script>

</body>



</html>
