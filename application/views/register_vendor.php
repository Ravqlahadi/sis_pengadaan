
<!DOCTYPE html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Dinas PU | Kabupaten Konawe Utara</title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg top-navigation">

  <div id="wrapper">
          <div id="" class="gray-bg">
            <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-md-1 col-md-offset-1" style="text-align:right">
              <img src="<?php echo base_url()?>/assets/img/logo-konut.png" style="height:100px;" alt="" />
            </div>
            <div class="col-md-8"  style="text-align:center;margin-top:10px" >
              <h1>Layanan Pengadaan Barang dan Jasa Pemerintah</h1> <h1 style="margin-top:-5px">Dinas Pekerjaan Umum Kabupaten Konawe Utara</h1>
            </div>
            <div class="col-md-1" style="padding-top:10px;text-align:left" >
              <img src="<?php echo base_url()?>/assets/img/logo-pu.png" style="height:100px;" alt="" />
            </div>
        </div>
          <div class="row border-bottom white-bg">
          <nav class="navbar navbar-static-top" role="navigation">
            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                <i class="fa fa-reorder"></i>
            </button>
              <div class="navbar-collapse collapse" id="navbar">
                <div class="row">
                  <div class="col-md-8 col-md-offset-1">
                    <ul class="nav navbar-nav">
                      <li>
                          <a href="<?php echo site_url()?>">
                              <i class="fa fa-home"></i> Beranda
                          </a>
                      </li>
                      <li>
                          <a href="#">
                              <i class="fa fa-newspaper-o"></i> Tentang Kami
                          </a>
                      </li>

                    </ul>
                  </div>
                </div>


              </div>
          </nav>
          </div>
          <div class="wrapper wrapper-content">
              <div class="container">
                <div class="row">
                  <div class="col-lg-12">
                  <div class="ibox float-e-margins">
                      <div class="ibox-title">
                          <h5>Register</h5>
                        </div>
                      <div class="ibox-content">

                        <div class="row">
                          <div class="col-md-12 text-center">
                            <h2><b>Daftar Penyedia</b></h2>
                          </div>
                        <form action="<?php echo site_url('home/form_daftar')?>" method="post">
                          <div class="col-md-12 form-horizontal" style="margin-top:20px">
                              <div class="col-md-12 form-regis">
                                <label class="col-sm-2 col-md-offset-1 control-label" >Username</label>
                                  <div class="col-sm-7"><input type="text" class="form-control" name="username"></div>
                              </div>
                              <div class="col-md-12  form-regis">
                                <label class="col-sm-2 col-md-offset-1 control-label">Password</label>
                                  <div class="col-sm-7"><input type="password" id="txtPws" class="form-control"></div>
                              </div>
                              <div class="col-md-12  form-regis">
                                <label class="col-sm-2 col-md-offset-1 control-label">Confirm Password</label>
                                  <div class="col-sm-7"><input type="password" id="txtConfirmPws" onkeyup="checkPasswordMatch()" class="form-control" name="password"></div>
                                  <div class="registrationFormAlert" id="divCheckPasswordMatch"></div>
                              </div>

                          </div>

                          <div class="col-md-10 col-md-offset-1" style="margin-top:20px;">
                            <h3>A. Data Administrasi</h3>
                          </div>

                          <div class="col-md-12 form-horizontal">

                              <div class="col-md-12 form-regis">
                                <label class="col-sm-2 col-md-offset-1 control-label" >Nama Perusahaan</label>
                                  <div class="col-sm-7"><input type="text" class="form-control" name="nama"></div>
                              </div>
                              <div class="col-md-12  form-regis">
                                <label class="col-sm-2 col-md-offset-1 control-label">Alamat</label>
                                  <div class="col-sm-7"><textarea class='form-control' name="alamat" placeholder=''></textarea>
                                  </div>
                              </div>
                              <div class="col-md-12  form-regis">
                                <label class="col-sm-2 col-md-offset-1 control-label">Nomor Telepon</label>
                                  <div class="col-sm-7"><input type="text" class="form-control" name="nomor_telepon"></div>
                              </div>
                              <div class="col-md-12  form-regis">
                                <label class="col-sm-2 col-md-offset-1 control-label">Email</label>
                                  <div class="col-sm-7"><input type="text" class="form-control" name="email"></div>
                              </div>

                          </div>

                          <div class="col-md-10 col-md-offset-1" style="margin-top:20px;">
                            <h3>B. Izin Usaha</h3>
                          </div>

                          <div class="col-md-12 form-horizontal">

                              <div class="col-md-12  form-regis">
                                <label class="col-sm-2 col-md-offset-1 control-label">Nomor Izin Usaha</label>
                                  <div class="col-sm-7"><input type="text" class="form-control" name="nomor_izin_usaha"></div>
                              </div>
                              <div class="col-md-12  form-regis">
                                <label class="col-sm-2 col-md-offset-1 control-label">Izin Usaha</label>
                                    <div class="col-sm-7"><input type="text" class="form-control" name="izin_usaha"></div>
                              </div>

                          </div>

                          <div class="col-md-10 col-md-offset-1" style="margin-top:20px;">
                            <h3>C. Data Keuangan</h3>
                          </div>

                          <div class="col-md-12 form-horizontal">

                              <div class="col-md-12  form-regis">
                                <label class="col-sm-2 col-md-offset-1 control-label">NPWP</label>
                                  <div class="col-sm-7"><input type="text" class="form-control" name="npwp"></div>
                              </div>
                              <div class="col-md-12  form-regis">
                                <label class="col-sm-2 col-md-offset-1 control-label">Bukti Pajak</label>
                                  <div class="col-sm-7"><input type="file" class="form-control" name="bukti_pajak"></div>
                              </div>
                              <div class="col-md-12  form-regis">
                                <label class="col-sm-2 col-md-offset-1 control-label">Susunan Kepemilikan Saham</label>
                                  <div class="col-sm-7"><input type="file" class="form-control" name="susunan_kepemilikan"></div>
                              </div>

                          </div>

                          <div class="col-md-10 col-md-offset-1" style="margin-top:20px;">
                            <h3>D. Landasan Hukum Perusahaan</h3>
                          </div>

                          <div class="col-md-12 form-horizontal">

                              <div class="col-md-12  form-regis">
                                <label class="col-sm-2 col-md-offset-1 control-label">No. Akta Perusahaan</label>
                                  <div class="col-sm-7"><input type="text" class="form-control" name="no_akta"></div>
                              </div>
                              <div class="col-md-12  form-regis">
                                <label class="col-sm-2 col-md-offset-1 control-label">Tanggal</label>
                                  <div class="col-sm-7"><input type="date" class="form-control" name="tanggal_akta"></div>
                              </div>
                              <div class="col-md-12  form-regis">
                                <label class="col-sm-2 col-md-offset-1 control-label">Nama Notaris</label>
                                  <div class="col-sm-7"><input type="text" class="form-control" name="nama_notaris"></div>
                              </div>
                              <div class="col-md-12  form-regis">
                                <label class="col-sm-2 col-md-offset-1 control-label">Pengurus Perusahaan</label>
                                  <div class="col-sm-7"><input type="file" class="form-control" name="pengurus_perusahaan"></div>
                              </div>

                          </div>

                          <div class="col-md-10 col-md-offset-1" style="margin-top:20px;">
                            <h3>E. Pengalaman Perusahaan</h3>
                          </div>

                          <div class="col-md-12 form-horizontal">

                              <div class="col-md-12  form-regis">
                                <label class="col-sm-2 col-md-offset-1 control-label">Pengalaman Perusahaan</label>
                                  <div class="col-sm-7"><input type="file" class="form-control" name="pengalaman_perusahaan"></div>
                              </div>

                          </div>

                          <div class="col-md-12">
                            <div class="col-md-8 col-md-offset-2"  style="margin-top:30px;float:right">
                              <button type='submit' class='btn btn-md btn-success '>Simpan</button>
                              <button type='submit' class='btn btn-md btn-warning '>Batal</button>
                            </div>
                          </div>
                        </div>


                      </form>
                      </div>
                  </div>

                  </div>

                </div>
              </div>
          </div>


          </div>
          </div>



    <!-- Mainly scripts -->
    <script src="<?php echo base_url()?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url()?>assets/js/plugins/pace/pace.min.js"></script>
    <script>
    function checkPasswordMatch() {
      var password = $("#txtPws").val();
      var confirmPassword = $("#txtConfirmPws").val();

      if (password != confirmPassword)
          $("#divCheckPasswordMatch").html("<span style='color:red'>Password Tidak Cocok!</span");
      else
          $("#divCheckPasswordMatch").html("Passwords Cocok.");
    }

    $(document).ready(function () {
     $("#txtConfirmPws").keyup(checkPasswordMatch);
    });
</script>

    <script type="text/javascript">
          $('#myModal').on('shown.bs.modal', function () {
      $('#myInput').focus()
      })
    </script>

</body>



</html>
