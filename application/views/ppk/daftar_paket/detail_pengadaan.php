<div class="wrapper wrapper-content">

  <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h4>Selamat Datang Admin Pejabat Pembuat Komitmen</h5>
          <div class="ibox float-e-margins">
              <div class="ibox-title text-center">
                  <h2><?php $k=$data_pengadaan[0];
                  echo $k->nama_pengadaan;
                  ?></h5>
                </div>
              <div class="ibox-content">
                <div class='row '>
                  <div class="tabs-container">
                       <ul class="nav nav-tabs">
                           <li class="active"><a data-toggle="tab" href="#tab-1">Informasi Lelang</a></li>
                           <li class=""><a data-toggle="tab" href="#tab-2">Lihat Pemenang Lelang</a></li>
                       </ul>
                       <div class="tab-content">
                           <div id="tab-1" class="tab-pane active">
                               <div class="panel-body">
                                 <div class="col-md-12 form-regis">
                                   <label class="col-sm-2 col-md-offset-1 control-label" >Kode Pengadaan</label>
                                     <div class="col-sm-7"><input type="text" class="form-control" name="id_pengadaan" readonly value="<?php echo $k->id_pengadaan;?>"></div>
                                 </div>
                                 <div class="col-md-12 form-regis">
                                   <label class="col-sm-2 col-md-offset-1 control-label" >Nama Pengadaan</label>
                                     <div class="col-sm-7"><input type="text" class="form-control" name="nama_pengadaan" readonly value="<?php echo $k->nama_pengadaan;?>"></div>
                                 </div>
                                 <div class="col-md-12 form-regis">
                                   <label class="col-sm-2 col-md-offset-1 control-label" >Jenis Pengadaan</label>

                                     <div class="col-sm-7"><input type="text" class="form-control" name="lokasi_pengadaan" readonly value="<?php echo $k->jenis_pengadaan;?>"></div>

                                 </div>

                                 <div class="col-md-12 form-regis">
                                   <label class="col-sm-2 col-md-offset-1 control-label" >Lokasi Pengerjaan</label>
                                     <div class="col-sm-7"><input type="text" class="form-control" name="lokasi_pengadaan" readonly value="<?php echo $k->lokasi_pekerjaan;?>"></div>
                                 </div>
                                 <div class="col-md-12 form-regis">
                                   <label class="col-sm-2 col-md-offset-1 control-label" >Jadwal Mulai Pekerjaan</label>
                                     <div class="col-sm-7"><input type="text" class="form-control" name="jadwal_mulai" readonly value="<?php echo $k->jadwal_pengadaan;?>"></div>
                                 </div>

                                 <div class="col-md-12 form-regis">
                                   <label class="col-sm-2 col-md-offset-1 control-label" >Dokumen</label>
                                     <div class="col-sm-7"><a class="btn btn-warning" target="_blank" href="<?php echo base_url()?>/upload/dokumen/<?php echo $k->dokumen;?>"><i class="fa fa-download"></i> Unduh</a></div>
                                 </div>

                               </div>
                           </div>
                           <div id="tab-2" class="tab-pane">
                               <div class="panel-body">
                                 <table class="table table-striped table-bordered">
                                    <thead>
                                      <tr>
                                        <th>No</th>
                                        <th>Nama Peserta</th>
                                        <th>Administrasi</th>
                                        <th>Teknis</th>
                                        <th>Harga Penawaran</th>
                                        <th>Pemenang</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>CV. Media Tama</td>
                                        <td>
                                          <div class='checkbox-inline'>
                                            <label>
                                                <input type='checkbox' name='' value='' checked disabled>

                                            </label>
                                          </div>
                                        </td>
                                        <td><div class='checkbox-inline'>
                                          <label>
                                            <input type='checkbox' name='' value='' checked disabled>

                                          </label>
                                        </div></td>
                                        <td>Rp.67550000</td>
                                        <td><div class='checkbox-inline'>
                                          <label>
                                            <input type='checkbox' name='' value='' checked disabled>

                                          </label>
                                        </div></td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>CV. Transnusa</td>
                                        <td>
                                          <div class='checkbox-inline'>
                                            <label>
                                              <input type='checkbox' name='' value='' checked disabled>

                                            </label>
                                          </div>
                                        </td>
                                        <td><div class='checkbox-inline'>
                                          <label>
                                            <input type='checkbox' name='' value='' checked disabled>

                                          </label>
                                        </div></td>
                                        <td>Rp.67450000</td>
                                        <td><div class='checkbox-inline'>
                                          <label>
                                            <input type='checkbox' name='' value=''>

                                          </label>
                                        </div></td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>PT. Nusjaya</td>
                                        <td>
                                          <div class='checkbox-inline'>
                                            <label>
                                              <input type='checkbox' name='' value='' checked disabled>

                                            </label>
                                          </div>
                                        </td>
                                        <td><div class='checkbox-inline'>
                                          <label>
                                            <input type='checkbox' name='' value='' checked disabled>

                                          </label>
                                        </div></td>
                                        <td>Rp.67450000</td>
                                        <td><div class='checkbox-inline'>
                                          <label>
                                            <input type='checkbox' name='' value=''>

                                          </label>
                                        </div></td>
                                      </tr>
                                    </tbody>
                                  </table>
                               </div>
                           </div>
                       </div>


                   </div>

                </div>
              </div>
          </div>

        </div>
      </div>
  </div>
</div>
