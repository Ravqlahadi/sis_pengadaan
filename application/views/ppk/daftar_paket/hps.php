<div class="wrapper wrapper-content">

  <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h4>Selamat Datang Admin Pejabat Pembuat Komitmen</h5>
            <div class="row">

                  <div class="col-md-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title text-center">
                            <h2>Detail HPS</h5>
                          </div>
                        <div class="ibox-content">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="table-responsive">
                                  <table class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead>
                                    <tr>
                                        <th>no</th>
                                        <th>Nama Barang & Jasa</th>
                                        <th>Satuan</th>
                                        <th>Volume</th>
                                        <th>Harga Satuan</th>
                                        <th>Total Harga</th>
                                        <th>Keterangan</th>
                                    </tr>
                                    </thead>
                                  </thead>
                                  <tbody>
                                    <?php
                                    $no=1;
                                    $total=0;
                                      foreach ($daftar_hps as $k) { ?>
                                        <tr>
                                          <td><?php echo $no;?></td>
                                          <td><?php echo $k->nama_barang;?></td>
                                          <td><?php echo $k->satuan_barang;?></td>
                                          <td><?php echo $k->volume_barang;?></td>
                                          <td><?php echo $k->harga_barang;?></td>
                                          <td><?php echo $k->harga_barang*$k->volume_barang;?></td>
                                          <td><?php //echo $k->keterangan;?></td>
                                        </tr>
                                      <?php
                                      $total = $total+($k->harga_barang*$k->volume_barang);

                                      $no++;
                                    } ?>
                                  </tbody>

                                  </table>
                                </div>

                            </div>

                          </div>
                          <div class="row" style="margin-bottom:20px">
                              <div class="col-md-4 col-md-offset-8" style="text-align:right">
                                <div class="row">
                                  <div class="col-md-6">
                                    <h3>Jumlah</h3>
                                  </div>
                                  <div class="col-md-6">
                                    <h3>Rp.<?php echo $total;?></h3>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-6">
                                    <h3>PPN 10%</h3>
                                  </div>
                                  <div class="col-md-6">
                                    <h3>Rp.<?php $ppn=($total*10)/100;echo $ppn?></h3>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-6">
                                    <h3>Total Nilai</h3>
                                  </div>
                                  <div class="col-md-6">
                                    <h3>Rp.<?php $total_nilai=$total+$ppn;echo $total_nilai;?></h3>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12" style="text-align:right">
                              <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal">
                                  <i class="fa fa-plus"></i> Tambah
                              </button>
                              <!-- Modal -->
                              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog modal-lg" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title" id="myModalLabel">Tambah HPS</h4>
                                    </div>
                                    <div class="modal-body">
                                      <div class="row">
                                        <form class="" action="<?php echo site_url('pokja/input_hps')?>" method="post" enctype="multipart/form-data">
                                          <div class="form-horizontal">

                                            <div class="col-md-12 form-regis">
                                              <label class="col-sm-2 col-md-offset-1 control-label" >Nama Barang</label>
                                                <div class="col-sm-7">
                                                  <select id="mySelect" class="form-control" name="id_barang" onchange="myFunction()">
                                                    <option value="">Pilih Barang/Jasa</option>
                                                      <?php foreach ($daftar_barang as $k) {

                                                        ?>
                                                          <option value="<?php echo $k->id_barang?>"><?php echo $k->nama_barang;?>
                                                    <?php

                                                      }?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12 form-regis">
                                              <label class="col-sm-2 col-md-offset-1 control-label" >Jumlah Barang</label>
                                                <div class="col-sm-7"><input type="text" id="volBar" class="form-control" name="volume_barang" onchange="tambhVol()">
                                                  <input type="hidden" id="volBar" class="form-control" name="id_hps" value="<?php echo $this->input->get('id')?>">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                              <div class="col-md-8 col-md-offset-2"  style="margin-top:30px;float:right">
                                                <button type='submit' class='btn btn-md btn-success '>Simpan</button>
                                                <button type='submit' class='btn btn-md btn-warning '>Batal</button>
                                              </div>
                                            </div>
                                          </div>
                                        </form>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <button type="button" class="btn btn-primary">Save changes</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <button type='submit' class='btn btn-md btn-success '><i class="fa fa-save"></i> Simpan</button>
                              <button  onclick="window.history.go(-1); return false;" type='' class='btn btn-md btn-warning '><i class="fa fa-back"></i>Kembali</button>
                            </div>

                          </div>
                        </div>
                    </div>
              </div>
            </div>

        </div>
        <div class="col-md-12">

        </div>
      </div>
  </div>
</div>
