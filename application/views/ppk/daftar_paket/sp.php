<div class="wrapper wrapper-content">

  <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h4>Selamat Datang Admin Pejabat Pembuat Komitmen</h5>
          <div class="ibox float-e-margins">
              <div class="ibox-title text-center">
                  <h2>Surat Permintaan</h5>
                </div>
              <div class="ibox-content">
                <div class="row">
                  <form class="" action="<?php echo site_url('ppk/input_sp')?>" method="post" enctype="multipart/form-data">
                      <div class="form-horizontal">
                        <div class="col-md-12 form-regis">
                          <label class="col-sm-2 col-md-offset-1 control-label" >No Surat</label>
                            <div class="col-sm-7"><input type="text" class="form-control" name="id_pengadaan"></div>
                        </div>
                        <div class="col-md-12 form-regis">
                          <label class="col-sm-2 col-md-offset-1 control-label" >Tanggal</label>
                            <div class="col-sm-7"><input type="text" class="form-control" name="nama_pengadaan"></div>
                        </div>


                        <div class="col-md-12 form-regis">
                          <label class="col-sm-2 col-md-offset-1 control-label" >Nama Perusahaan</label>
                            <div class="col-sm-7"><input type="text" class="form-control" name="lokasi_pengadaan"></div>
                        </div>
                        <div class="col-md-12 form-regis">
                          <label class="col-sm-2 col-md-offset-1 control-label" >Nama Pengadaan</label>
                            <div class="col-sm-7"><input type="text" class="form-control" name="jadwal_mulai"></div>
                        </div>
                        <div class="col-md-12 form-regis">
                          <label class="col-sm-2 col-md-offset-1 control-label" >Nilai Kontrak</label>
                            <div class="col-sm-7"><input type="text" class="form-control" name="jadwal_selesai"></div>
                        </div>
                        <div class="col-md-12 form-regis">
                          <label class="col-sm-2 col-md-offset-1 control-label" >Keterangan</label>
                            <div class="col-sm-7">
                              <table class="table table-striped table-bordered table-xs">
                                <tr>
                                  <th style="width:10%">No</th>
                                  <th>Nama Barang</th>
                                  <th>Volume</th>
                                </tr>
                                <tr>
                                  <td>1</td>
                                  <td>Meja
                                  <td>20 Buah</td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td>Kursi</td>
                                  <td>20 Buah</td>
                                </tr>

                              </table>
                            </div>
                        </div>
                        <div class="col-md-12 form-regis">
                          <label class="col-sm-2 col-md-offset-1 control-label" >Keterangan</label>
                            <div class="col-sm-7"><textarea type="text" class="form-control" name="jadwal_selesai"></textarea></div>
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-8 col-md-offset-2"  style="margin-top:30px;float:right">
                            <button type='submit' class='btn btn-md btn-success '>Simpan</button>
                            <button type='' class='btn btn-md btn-warning '>Cetak</button>
                          </div>
                        </div>
                      </div>
                  </form>

                </div>
              </div>
          </div>

        </div>
      </div>
  </div>
</div>
