<div class="wrapper wrapper-content">

  <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h4>Selamat Datang Admin Pejabat Pembuat Komitmen</h5>
          <div class="ibox float-e-margins">
              <div class="ibox-title text-center">
                  <h2>Daftar Paket</h5>
                </div>
              <div class="ibox-content">
                <div class="row">

                  <div class="col-md-12">
                    <div class="">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                          <thead>
                          <tr>
                              <th>Kode</th>
                              <th>Nama Pengadaan</th>
                              <th>Jenis Pengadaan</th>
                              <th>Jadwal</th>
                              <th>Lokasi</th>
                              <th>HPS</th>
                              <th>Dokumen</th>
                              <th>Status</th>
                              <th>Aksi</th>
                          </tr>
                          </thead>
                          <tbody>
                            <?php
                              foreach ($daftar_pengadaan as $k) { ?>
                                <tr>
                                  <td><?php echo $k->id_pengadaan;?></td>
                                  <td><?php echo $k->nama_pengadaan;?></td>
                                  <td><?php echo $k->jenis_pengadaan;?></td>
                                  <td><?php echo $k->jadwal_pengadaan;?></td>
                                  <td><?php echo $k->lokasi_pekerjaan;?></td>
                                  <td>Rp.<?php echo $k->nilai_hps;?></td>
                                  <td><a href="<?php echo base_url('upload/dokumen')."/".$k->dokumen?>"> <?php echo $k->dokumen;?></a></td>
                                  <td><?php echo $k->status;?></td>
                                  <td>
                                    <div class="dropdown">
                                      <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        Aksi
                                        <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        <li><a href="<?php echo site_url('ppk/detail_hps').'?id='.$k->id_pengadaan?>">Detail HPS</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="#" data-toggle="modal" data-target="#myModal<?php echo $k->id_pengadaan?>">Terima</a></li>
                                        <div class="modal fade" id="myModal<?php echo $k->id_pengadaan?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                          <div class="modal-dialog  modal-sm" role="document">
                                            <div class="modal-content">

                                              <div class="modal-body text-center">
                                                <h2>Terima Pengadaan</h2>
                                              </div>
                                              <div class="modal-footer">
                                                <a href="<?php echo site_url('ppk/terima_pengadaan')."?id=".$k->id_pengadaan;?>"><button type="button" class="btn btn-success">Terima</button></a>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>

                                              </div>
                                            </div>
                                          </div>
                                          </div>
                                        <li><a href="#">Tolak</a></li>
                                      </ul>
                                    </div>
                                  </td>
                                </tr>
                              <?php } ?>
                          </tbody>

                        </table>
                      </div>
                  </div>
                </div>
              </div>
          </div>

        </div>
      </div>
  </div>
</div>
