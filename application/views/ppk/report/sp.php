<?php

ob_start();

$template = '<page backbottom="5mm" backimg="" backimgw="100%" backimgx="100%" backimgy="100%" backleft="10mm" backright="15mm" backtop="15mm"> <bookmark level="0" title="Surat"></bookmark>
<table border="0" cellpadding="0" cellspacing="0" style="line-height: 1.1; width: 100%;">
	<tbody>
		<tr>
			<td style="width: 10%; text-align: center;"><img alt="" src="http://localhost/sis_pengadaan//assets/img/logo-konut.png" style="line-height: 15.1111px; width: 80px; height: 91px;" /></td>
			<td style="width: 80%; text-align: center;"><span style="font-size:18px;"><span style="text-align: center;"><strong>PEMERINTAH PROVINSI SULAWESI TENGGARA<br />
			DINAS PEKERJAAN UMUM<br />
			KABUPATEN KONAWE UTARA</strong></span></span><br />
			<span style="text-align: center; font-size: 12px;">Jalan Poros Kolaka-Kendari Desa Orawa Kec. Tirawuta Kab. Kolaka Timur </span><br />
			<span style="color: rgb(51, 153, 255); font-size: 12px; text-align: center;">Email : blabla@gmail.com</span></td>
			<td style="width: 10%;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3" style="width: 100%; text-align: center;">
			<hr />
			<p><u><span style="font-size: 16px;"><b>SURAT PESANAN (SP)</b></span></u><br />
			<span style="font-size:14px;"><span style="text-align: center;">Nomor : nomor.surat</span></span><br style="text-align: center;" />
			<span style="text-align: center; font-size: 14px;">Paket Pekerjaan : paket.pekerjaan</span></p>
			</td>
		</tr>
	</tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" style="line-height: 1.6; width: 100%;">
	<tbody>
		<tr>
			<td style="width: 15%;">&nbsp;</td>
			<td style="width: 5%;">&nbsp;</td>
			<td style="width: 20%;">&nbsp;</td>
			<td style="width: 60%;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 100%; line-height: 15px; text-align: justify;">Yang bertanda tangan dibawah ini:</td>
		</tr>
	</tbody>
</table>
</page>

<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
	<tbody>
		<tr>
			<td style="width: 5%; text-align: center;">&nbsp;</td>
			<td style="width: 15%;">&nbsp;</td>
			<td style="width: 5%;">&nbsp;</td>
			<td style="width: 75%;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">Nama</td>
			<td style="width: 5%; vertical-align: top;">:</td>
			<td style="width: 75%; vertical-align: top;">nama.ppk</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">Jabatan</td>
			<td style="width: 5%; vertical-align: top;">:</td>
			<td style="width: 75%; vertical-align: top; text-align: justify;">Pejabat Pembuat Komitmen (PPK) berdasarkan Keputusan Pengguna Anggaran/Kuasa Pengguna Anggaran<sup>1</sup>) Nomor &hellip;&hellip;&hellip;.. tanggal &hellip;&hellip;&hellip;&hellip;&hellip; tentang &hellip;&hellip;&hellip;SK Penunjukkan PPK.&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">Alamat</td>
			<td style="width: 5%; vertical-align: top;">:</td>
			<td style="width: 75%; vertical-align: top;">alamat.ppk</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">&nbsp;</td>
			<td style="width: 5%; vertical-align: top;">&nbsp;</td>
			<td style="width: 75%; vertical-align: top;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">&nbsp;</td>
			<td style="width: 5%; vertical-align: top;">&nbsp;</td>
			<td style="width: 75%; vertical-align: top;">Yang selanjutnya disebut sebagai Pejabat Pembuat Komitmen;&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">&nbsp;</td>
			<td style="width: 5%; vertical-align: top;">&nbsp;</td>
			<td style="width: 75%; vertical-align: top;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td colspan="3" rowspan="1" style="width: 95%; vertical-align: top; text-align: justify;">Berdasarkan Surat Perintah Kerja (SPK) .................... nomor ........................... tanggal ......................., bersama ini memerintahkan :</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">&nbsp;</td>
			<td style="width: 5%; vertical-align: top;">&nbsp;</td>
			<td style="width: 75%; vertical-align: top;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">Nama</td>
			<td style="width: 5%; vertical-align: top;">:</td>
			<td style="width: 75%; vertical-align: top;">nama.penyedia</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">Jabatan</td>
			<td style="width: 5%; vertical-align: top;">:</td>
			<td style="width: 75%; vertical-align: top;">jabatan.penyedia</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">Alamat</td>
			<td style="width: 5%; vertical-align: top;">:</td>
			<td style="width: 75%; vertical-align: top;">alamat.penyedia</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">&nbsp;</td>
			<td style="width: 5%; vertical-align: top;">&nbsp;</td>
			<td style="width: 75%; vertical-align: top;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">&nbsp;</td>
			<td style="width: 5%; vertical-align: top;">&nbsp;</td>
			<td style="width: 75%; vertical-align: top;">Yang selanjutnya disebut sebagai Penyedia Barang</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">&nbsp;</td>
			<td style="width: 5%; vertical-align: top;">&nbsp;</td>
			<td style="width: 75%; vertical-align: top;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td colspan="3" rowspan="1" style="width: 95%; vertical-align: top; text-align: justify;">Untuk mengirimkan barang dengan memperhatikan ketentuan-ketentuan sebagai berikut :</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td colspan="3" style="width: 95%; vertical-align: top; text-align: justify;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; vertical-align: top; text-align: center;">1.</td>
			<td colspan="3" style="width: 95%; vertical-align: top; text-align: justify;"><u>Rincian Barang</u></td>
		</tr>
		<tr>
			<td style="width: 5%; vertical-align: top; text-align: justify;">&nbsp;</td>
			<td colspan="3" style="width: 95%; vertical-align: top; text-align: justify;">
			<table border="1" cellpadding="0" cellspacing="0" style="width:100%;">
				<tbody>
					<tr>
						<td style="width: 5%; text-align: center;"><strong>No</strong></td>
						<td style="width: 25%; text-align: center;"><strong><em>Jenis Barang</em></strong></td>
						<td style="width: 15%; text-align: center;"><strong>Satuan Ukuran</strong></td>
						<td style="width: 15%; text-align: center;"><strong>Kuantitas</strong></td>
						<td style="text-align: center; width: 20%;"><strong>Harga Satuan</strong></td>
						<td style="width: 20%; text-align: center;"><strong>Total Harga</strong></td>
					</tr>
					<tr>
						<td style="width: 5%; text-align: center;">&nbsp;</td>
						<td style="width: 25%; text-align: center;">&nbsp;</td>
						<td style="width: 15%; text-align: center;">&nbsp;</td>
						<td style="width: 15%; text-align: center;">&nbsp;</td>
						<td style="text-align: center; width: 20%;">&nbsp;</td>
						<td style="width: 20%; text-align: center;">&nbsp;</td>
					</tr>
					<tr>
						<td style="width: 5%; text-align: center;">&nbsp;</td>
						<td style="width: 25%; text-align: center;">&nbsp;</td>
						<td style="width: 15%; text-align: center;">&nbsp;</td>
						<td style="width: 15%; text-align: center;">&nbsp;</td>
						<td style="text-align: center; width: 20%;">&nbsp;</td>
						<td style="width: 20%; text-align: center;">&nbsp;</td>
					</tr>
					<tr>
						<td style="width: 5%; text-align: center;">&nbsp;</td>
						<td style="width: 25%; text-align: center;">&nbsp;</td>
						<td style="width: 15%; text-align: center;">&nbsp;</td>
						<td style="width: 15%; text-align: center;">&nbsp;</td>
						<td style="text-align: center; width: 20%;">&nbsp;</td>
						<td style="width: 20%; text-align: center;">&nbsp;</td>
					</tr>
					<tr>
						<td style="width: 5%; text-align: center;">&nbsp;</td>
						<td style="width: 25%; text-align: center;">&nbsp;</td>
						<td style="width: 15%; text-align: center;">&nbsp;</td>
						<td style="width: 15%; text-align: center;">&nbsp;</td>
						<td style="text-align: center; width: 20%;">&nbsp;</td>
						<td style="width: 20%; text-align: center;">&nbsp;</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr>
			<td style="width: 5%; vertical-align: top; text-align: justify;">&nbsp;</td>
			<td colspan="3" style="width: 95%; vertical-align: top; text-align: justify;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; vertical-align: top; text-align: center;">2.</td>
			<td colspan="3" style="width: 95%; vertical-align: top; text-align: justify;">Tanggal Barang diterima : tanggal.terima.barang&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; vertical-align: top; text-align: center;">3.</td>
			<td colspan="3" style="width: 95%; vertical-align: top; text-align: justify;"><u>Syarat-syarat pekerjaan</u> : sesuai dengan persyaratan dan ketentuan Kontrak;&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; vertical-align: top; text-align: center;">4.</td>
			<td colspan="3" style="width: 95%; vertical-align: top; text-align: justify;">Waktu penyelesaian : selama ___ (__________) hari kalender/bulan/tahun dan pekerjaan harus sudah selesai pada tanggal __________&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; vertical-align: top; text-align: center;">5.</td>
			<td colspan="3" style="width: 95%; vertical-align: top; text-align: justify;">Alamat pengiriman barang : __________________________________ . Denda: Terhadap setiap hari keterlambatan penyelesaian pekerjaan Penyedia Jasa akan dikenakan Denda.</td>
		</tr>
		<tr>
			<td style="width: 5%; vertical-align: top; text-align: center;">6.</td>
			<td colspan="3" style="width: 95%; vertical-align: top; text-align: justify;">Keterlambatan sebesar 1/1000 (satu per seribu) dari Nilai Kontrak atau bagian tertentu dari Nilai Kontrak sebelum PPN sesuai dengan persyaratan dan ketentuan Kontrak.&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td colspan="3" style="width: 95%; vertical-align: top; text-align: justify;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td colspan="3" style="width: 95%; vertical-align: top; text-align: justify;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td colspan="3" style="width: 95%; vertical-align: top; text-align: right;">Konawe Utara, tanggal.penetapan</td>
		</tr>
	</tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
	<tbody>
		<tr>
			<td style="width: 50%;">&nbsp;</td>
			<td style="width: 50%;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 50%;">&nbsp;</td>
			<td style="width: 50%;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" style="width: 50%; text-align: center;"><strong>Menerima dan menyetujui :&nbsp;</strong></td>
		</tr>
		<tr>
			<td style="width: 50%;">&nbsp;</td>
			<td style="width: 50%;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 50%; text-align: center;">
			<p style="text-align: center;">Untuk dan atas nama<br />
			Pejabat Pembuat Komitmen</p>

			<p style="text-align: center;">&nbsp;</p>

			<p style="text-align: center;">&nbsp;</p>

			<p style="text-align: center;"><u style="text-align: center;">nama.ppk</u><br style="text-align: center;" />
			<span style="text-align: center;">jabatan.ppk</span><br style="text-align: center;" />
			<span style="text-align: center;">nip.ppk</span></p>
			</td>
			<td style="width: 50%; text-align: center;">
			<p style="text-align: center;">Untuk dan atas nama&nbsp;<br />
			nama.penyedia&nbsp;</p>

			<p style="text-align: center;">&nbsp;</p>

			<p style="text-align: center;">&nbsp;</p>

			<p style="text-align: center;"><u>nama.penyedia&nbsp;</u><br />
			jabatan.penyedia</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

';

echo $template;
$content = ob_get_contents();
ob_clean();
try
{
   $html2pdf = new HTML2PDF('L', 'A4', 'en');
   $html2pdf->pdf->SetDisplayMode('fullpage');
   $html2pdf->setDefaultFont('Arial');
   $html2pdf->writeHTML($content);
   $html2pdf->Output('tes.pdf');
}
catch(HTML2PDF_exception $e) {
   echo $e;
   exit;
}
?>
