<?php

ob_start();

$template = '<page backbottom="5mm" backimg="" backimgw="100%" backimgx="100%" backimgy="100%" backleft="10mm" backright="15mm" backtop="15mm"> <bookmark level="0" title="Surat"></bookmark>
<table border="0" cellpadding="0" cellspacing="0" style="line-height: 1.1; width: 100%;">
	<tbody>
		<tr>
			<td style="width: 10%; text-align: center;"><img alt="" src="http://localhost/sis_pengadaan//assets/img/logo-konut.png" style="line-height: 15.1111px; width: 80px; height: 91px;" /></td>
			<td style="width: 80%; text-align: center;"><span style="font-size:18px;"><span style="text-align: center;"><strong>PEMERINTAH PROVINSI SULAWESI TENGGARA<br />
			DINAS PEKERJAAN UMUM<br />
			KABUPATEN KONAWE UTARA</strong></span></span><br />
			<span style="text-align: center; font-size: 12px;">Jalan Poros Kolaka-Kendari Desa Orawa Kec. Tirawuta Kab. Kolaka Timur </span><br />
			<span style="color: rgb(51, 153, 255); font-size: 12px; text-align: center;">Email : blabla@gmail.com</span></td>
			<td style="width: 10%;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3" style="width: 100%; text-align: center;">
			<hr />
			<p><u><span style="font-size: 16px;"><b>BERITA ACARA SERAH TERIMA LAPORAN</b></span></u><br />
			<span style="font-size:14px;"><span style="text-align: center;">Nomor : nomor.surat</span></span></p>
			</td>
		</tr>
	</tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" style="line-height: 1.6; width: 100%;">
	<tbody>
		<tr>
			<td style="width: 15%;">&nbsp;</td>
			<td style="width: 5%;">&nbsp;</td>
			<td style="width: 20%;">&nbsp;</td>
			<td style="width: 60%;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 100%; line-height: 15px; text-align: justify;">Pada hari ini hari.penetapan, tanggal tanggal.penetapan bulan bulan.penetapan tahun tahun.penetapan , kami yang bertanda tangan dibawah ini:&nbsp;</td>
		</tr>
	</tbody>
</table>
</page>

<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
	<tbody>
		<tr>
			<td style="width: 5%; text-align: center;">&nbsp;</td>
			<td style="width: 15%;">&nbsp;</td>
			<td style="width: 5%;">&nbsp;</td>
			<td style="width: 75%;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center;">&nbsp;</td>
			<td style="width: 15%;">&nbsp;</td>
			<td style="width: 5%;">&nbsp;</td>
			<td style="width: 75%;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">1.</td>
			<td style="width: 15%; vertical-align: top;">Nama</td>
			<td style="width: 5%; vertical-align: top;">:</td>
			<td style="width: 75%; vertical-align: top;">nama.ppk</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">NIP</td>
			<td style="width: 5%; vertical-align: top;">:</td>
			<td style="width: 75%; vertical-align: top;">nip.ppk</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">Jabatan</td>
			<td style="width: 5%; vertical-align: top;">:</td>
			<td style="width: 75%; vertical-align: top; text-align: justify;">Pejabat Pembuat Komitmen (PPK) berdasarkan Keputusan Pengguna Anggaran/Kuasa Pengguna Anggaran<sup>1</sup>) Nomor &hellip;&hellip;&hellip;.. tanggal &hellip;&hellip;&hellip;&hellip;&hellip; tentang &hellip;&hellip;&hellip;SK Penunjukkan PPK.&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">&nbsp;</td>
			<td style="width: 5%; vertical-align: top;">&nbsp;</td>
			<td style="width: 75%; vertical-align: top;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">&nbsp;</td>
			<td style="width: 5%; vertical-align: top;">&nbsp;</td>
			<td style="width: 75%; vertical-align: top;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">&nbsp;</td>
			<td style="width: 5%; vertical-align: top;">&nbsp;</td>
			<td style="width: 75%; vertical-align: top;">Yang selanjutnya disebut sebagai PIHAK KESATU</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">&nbsp;</td>
			<td style="width: 5%; vertical-align: top;">&nbsp;</td>
			<td style="width: 75%; vertical-align: top;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">&nbsp;</td>
			<td style="width: 5%; vertical-align: top;">&nbsp;</td>
			<td style="width: 75%; vertical-align: top;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">2.</td>
			<td style="width: 15%; vertical-align: top;">Nama</td>
			<td style="width: 5%; vertical-align: top;">:</td>
			<td style="width: 75%; vertical-align: top;">nama.penyedia</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">Nip</td>
			<td style="width: 5%; vertical-align: top;">:</td>
			<td style="width: 75%; vertical-align: top;">nip.penyedia</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">Jabatan</td>
			<td style="width: 5%; vertical-align: top;">:</td>
			<td style="width: 75%; vertical-align: top;">jabatan.penyedia</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">&nbsp;</td>
			<td style="width: 5%; vertical-align: top;">&nbsp;</td>
			<td style="width: 75%; vertical-align: top;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">&nbsp;</td>
			<td style="width: 5%; vertical-align: top;">&nbsp;</td>
			<td style="width: 75%; vertical-align: top;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">&nbsp;</td>
			<td style="width: 5%; vertical-align: top;">&nbsp;</td>
			<td style="width: 75%; vertical-align: top;">Yang selanjutnya disebut sebaagai PIHAK KEDUA&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">&nbsp;</td>
			<td style="width: 5%; vertical-align: top;">&nbsp;</td>
			<td style="width: 75%; vertical-align: top;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 15%; vertical-align: top;">&nbsp;</td>
			<td style="width: 5%; vertical-align: top;">&nbsp;</td>
			<td style="width: 75%; vertical-align: top;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4" style="width: 100%; vertical-align: top;">Untuk pekerjaan :</td>
		</tr>
	</tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
	<tbody>
		<tr>
			<td style="width: 30%;">&nbsp;</td>
			<td style="width: 5%; text-align: center;">&nbsp;</td>
			<td style="width: 65%;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 30%;">&nbsp;</td>
			<td style="width: 5%; text-align: center;">&nbsp;</td>
			<td style="width: 65%;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 30%; vertical-align: top;">Nama Pekerjaan&nbsp;</td>
			<td style="width: 5%; text-align: center; vertical-align: top;">:</td>
			<td style="width: 65%; vertical-align: top;">nama.pekerjaan</td>
		</tr>
		<tr>
			<td style="width: 30%; vertical-align: top;">Nomor Kontrak/SPK</td>
			<td style="width: 5%; text-align: center; vertical-align: top;">:</td>
			<td style="width: 65%; vertical-align: top;">nomor.kontrak</td>
		</tr>
		<tr>
			<td style="width: 30%; vertical-align: top;">Tanggal Kontrak/SPK</td>
			<td style="width: 5%; text-align: center; vertical-align: top;">:</td>
			<td style="width: 65%; vertical-align: top;">tanggal.kontrak</td>
		</tr>
		<tr>
			<td style="width: 30%; vertical-align: top;">Lama/Tanggal Pelaksanaan&nbsp;</td>
			<td style="width: 5%; text-align: center; vertical-align: top;">:</td>
			<td style="width: 65%; vertical-align: top;">tanggal.mulai - tanggal.selesai</td>
		</tr>
		<tr>
			<td style="width: 30%; vertical-align: top;">&nbsp;</td>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 65%; vertical-align: top;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 30%; vertical-align: top;">&nbsp;</td>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 65%; vertical-align: top;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3" style="width: 100%; vertical-align: top; text-align: justify;">Dengan memperhatikan Berita Acara Pemeriksaan Hasil Pekerjaan Nomor isikan nomor Berita Acara Pemeriksaan Hasil Pekerjaan dari PPHP tanggal &hellip;&hellip;&hellip; beserta data dukungnya (format 4), maka dengan ini menyatakan sebagai berikut :&nbsp;</td>
		</tr>
	</tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
	<tbody>
		<tr>
			<td style="width: 5%;">&nbsp;</td>
			<td style="width: 95%;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">1.</td>
			<td style="width: 95%; text-align: justify; vertical-align: top;">PIHAK KEDUA melakukan penyerahan laporan akhir terhadap hasil pengadaan jasa konsultansi dimaksud diatas kepada PIHAK KESATU.</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">2.</td>
			<td style="width: 95%; text-align: justify; vertical-align: top;">PIHAK KESATU menerima penyerahan laporan akhir hasil pengadaan jasa konsultansi dimaksud diatas dari PIHAK KEDUA</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">3.</td>
			<td style="width: 95%; text-align: justify; vertical-align: top;">Hal-hal lain yang berkenaan dengan pasca serah terima laporan akhir ini tetap mengacu kepada ketentuan dalam kontrak/SPK dimaksud dan ketentuan peraturan yang berlaku.</td>
		</tr>
		<tr>
			<td style="width: 5%; text-align: center; vertical-align: top;">&nbsp;</td>
			<td style="width: 95%; text-align: justify; vertical-align: top;">&nbsp;</td>
		</tr>
	</tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
	<tbody>
		<tr>
			<td colspan="2" style="width: 100%; text-align: justify;">Demikian Berita Acara Serah Terima Laporan Akhir ini dibuat dengan sebenarnya guna bahan selanjutnya.</td>
		</tr>
		<tr>
			<td style="width: 50%;">&nbsp;</td>
			<td style="width: 50%;">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 50%; text-align: center;">
			<p style="text-align: center;">Yang Menyerahkan:<br />
			PIHAK KEDUA<br />
			Nama Penyedia,&nbsp;</p>

			<p style="text-align: center;">&nbsp;</p>

			<p style="text-align: center;">&nbsp;</p>

			<p style="text-align: center;"><u>nama.penyedia</u><br />
			jabatan.penyedia</p>
			</td>
			<td style="width: 50%; text-align: center;">
			<p style="text-align: center;">Yang Menerima:<br />
			PIHAK PERTAMA<br />
			Pejabat Pembuat Komitmen,&nbsp;&nbsp;</p>

			<p style="text-align: center;">&nbsp;</p>

			<p style="text-align: center;">&nbsp;</p>

			<p style="text-align: center;"><u>nama.ppk</u><br />
			jabatan.ppk</p>
			</td>
		</tr>
		<tr>
			<td style="width: 50%; text-align: center;">&nbsp;</td>
			<td style="width: 50%; text-align: center;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" style="width: 100%;"><em>Catatan:&nbsp;</em></td>
		</tr>
		<tr>
			<td colspan="2" style="width: 100%;"><em>Berita acara dibuat dalam rangkap dua, untuk disimpan Penyedia dibubuhi materai Rp. 6.000 pada kolom tanda tangan PPK, untuk disimpan PPK dibubuhi materai Rp. 6.000 dan cap pada kolom Penyedia.</em></td>
		</tr>
		<tr>
			<td colspan="2" style="width: 100%;"><em><strong>Perhatian </strong>: Format ini untuk pekerjaan Jasa Konsultansi.</em></td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

';

echo $template;
$content = ob_get_contents();
ob_clean();
try
{
   $html2pdf = new HTML2PDF('L', 'A4', 'en');
   $html2pdf->pdf->SetDisplayMode('fullpage');
   $html2pdf->setDefaultFont('Arial');
   $html2pdf->writeHTML($content);
   $html2pdf->Output('tes.pdf');
}
catch(HTML2PDF_exception $e) {
   echo $e;
   exit;
}
?>
