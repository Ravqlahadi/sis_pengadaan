<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ppk extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    $level=$this->session->userdata('level');
    if($level!='ppk' || $level==""){
      redirect('auth');
    }
  }

  function index()
  {
    $this->beranda();
  }



  public function beranda(){
    $data['menu']='beranda';
    $this->load->view('ppk/index',$data);
  }
  public function daftar_paket(){
    $data['menu']='daftar_paket';
    $this->load->view('ppk/index',$data);
  }
  public function daftar_vendor(){
    $data['menu']='beranda';
    $this->load->view('ppk/index',$data);
  }


  public function logout() {
    $this->session->unset_userdata('username');
    $this->session->unset_userdata('level');
    session_destroy();
    redirect(site_url());
  }


}
