<div class="wrapper wrapper-content">

  <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h4>Selamat Datang Admin Pejabat Pembuat Komitmen</h5>
          <div class="ibox float-e-margins">
              <div class="ibox-title text-center">
                  <h2>Daftar Paket Lelang Yang Aktif</h5>
                </div>
              <div class="ibox-content">
                <div class="">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                      <thead>
                      <tr>
                        <th>Kode Pengadaan</th>
                        <th>Nama Pengadaan</th>
                        <th>Vendor Pemenang</th>
                        <th>Tanggal(s)</th>
                        <th>Jumlah Peserta</th>
                          <th>Aksi</th>
                      </tr>
                      </thead>
                      <tbody>
                        <?php
                          foreach ($daftar_pengadaan as $k) { ?>
                            <tr>
                              <td><?php echo $k->id_pengadaan;?></td>
                              <td><?php echo $k->nama_pengadaan;?></td>
                              <td><?php echo $k->nama_perusahaan;?></td>
                              <td><?php echo $k->jadwal_pengadaan;?></td>
                              <td><?php echo $k->peserta;?></td>
                              <td>
                                <div class="dropdown">
                                  <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    Aksi
                                    <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <li><a href="<?php echo site_url('ppk/detail_hps').'?id='.$k->id_pengadaan?>">Detail HPS</a></li>
                                    <li><a href="<?php echo site_url('ppk/detail_pengadaan').'?id='.$k->id_pengadaan?>">Detail Pengadaan</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="<?php echo site_url('ppk/spmk').'?id='.$k->id_pengadaan?>">SPMK</a></li>
                                    <li><a href="<?php echo site_url('ppk/sp').'?id='.$k->id_pengadaan?>">Surat Permintaan</a></li>
                                    <li><a href="<?php echo site_url('ppk/bstp').'?id='.$k->id_pengadaan?>">BSTP</a></li>
                                  <!--  <li><a href="#">Tolak</a></li> -->
                                  </ul>
                                </div>
                              </td>
                            </tr>
                          <?php } ?>
                      </tbody>

                    </table>
                  </div>

              </div>
          </div>

        </div>
      </div>
  </div>
</div>
