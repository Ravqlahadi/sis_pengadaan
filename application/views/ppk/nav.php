<div id="" class="gray-bg">
  <div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-md-1 col-md-offset-1" style="text-align:right">
    <img src="<?php echo base_url()?>/assets/img/logo-konut.png" style="height:100px;" alt="" />
  </div>
  <div class="col-md-8"  style="text-align:center;margin-top:10px" >
    <h1>Layanan Pengadaan Barang dan Jasa Pemerintah</h1> <h1 style="margin-top:-5px">Dinas Pekerjaan Umum Kabupaten Konawe Utara</h1>
  </div>
  <div class="col-md-1" style="padding-top:10px;text-align:left" >
    <img src="<?php echo base_url()?>/assets/img/logo-pu.png" style="height:100px;" alt="" />
  </div>
</div>
<div class="row border-bottom white-bg">
<nav class="navbar navbar-static-top" role="navigation">
  <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
      <i class="fa fa-reorder"></i>
  </button>
    <div class="navbar-collapse collapse" id="navbar">
      <div class="row">
        <div class="col-md-8 col-md-offset-1">
          <ul class="nav navbar-nav">
            <li>
                <a href="<?php echo site_url()?>">
                    <i class="fa fa-home"></i> Beranda
                </a>
            </li>
            <li>
                <a href="<?php echo site_url('ppk/daftar_paket')?>">
                    <i class="fa fa-calendar-o"></i> Daftar Paket
                </a>
            </li>
            <li>
                  <a href="<?php echo site_url('ppk/daftar_vendor')?>">
                    <i class="fa fa-building"></i> Daftar Vendor
                </a>
            </li>
          
            <li>
                <a href="#">
                    <i class="fa fa-newspaper-o"></i> Tentang Kami
                </a>
            </li>
            <li>
                <a href="<?php echo site_url('ppk/logout')?>">
                    <i class="fa fa-sign-out"></i> Logout
                </a>
            </li>

          </ul>
        </div>
      </div>


    </div>
</nav>
</div>
