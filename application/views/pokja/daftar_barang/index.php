<div class="wrapper wrapper-content">

  <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h4>Selamat Datang Admin Pejabat barang</h5>
          <div class="ibox float-e-margins">
              <div class="ibox-title text-center">
                  <h2>Daftar Barang dan Jasa</h5>
                </div>
              <div class="ibox-content">
                <div class="row">
                  <div class="col-md-12" style="margin-bottom:20px">
                    <div class="row">
                      <div class="col-md-3">
                        <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-plus"></i> Tambah
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Tambah Barang & Jasa</h4>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                                  <form class="" action="<?php echo site_url('pokja/input_barang')?>" method="post" enctype="multipart/form-data">
                                    <div class="form-horizontal">
                                      <div class="col-md-12 form-regis">
                                        <label class="col-sm-2 col-md-offset-1 control-label" >Kode Barang</label>
                                          <div class="col-sm-7"><input type="text" class="form-control" name="id_barang"></div>
                                      </div>
                                      <div class="col-md-12 form-regis">
                                        <label class="col-sm-2 col-md-offset-1 control-label" >Nama Barang</label>
                                          <div class="col-sm-7"><input type="text" class="form-control" name="nama_barang"></div>
                                      </div>
                                      <div class="col-md-12 form-regis">
                                        <label class="col-sm-2 col-md-offset-1 control-label" >Satuan Barang</label>
                                          <div class="col-sm-7"><input type="text" class="form-control" name="satuan_barang"></div>
                                      </div>

                                      <div class="col-md-12 form-regis">
                                        <label class="col-sm-2 col-md-offset-1 control-label" >Harga Barang</label>
                                          <div class="col-sm-7"><input type="text" class="form-control" name="harga_barang"></div>
                                      </div>

                                      <div class="col-md-12">
                                        <div class="col-md-8 col-md-offset-2"  style="margin-top:30px;float:right">
                                          <button type='submit' class='btn btn-md btn-success '>Simpan</button>
                                          <button type='submit' class='btn btn-md btn-warning '>Batal</button>
                                        </div>
                                      </div>
                                    </div>
                                  </form>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                          <thead>
                          <tr>
                              <th>Kode</th>
                              <th>Nama Barang</th>
                              <th>Satuan Barang</th>
                              <th>Harga</th>
                              <th>Aksi</th>
                          </tr>
                          </thead>
                          <tbody>
                            <?php
                              foreach ($daftar_barang as $k) { ?>
                                <tr>
                                  <td><?php echo $k->id_barang;?></td>
                                  <td><?php echo $k->nama_barang;?></td>
                                  <td><?php echo $k->satuan_barang;?></td>                                  
                                  <td><?php echo $k->harga_barang;?></td>
                                  <td>
                                    <div class="dropdown">
                                      <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        Aksi
                                        <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                          <li><a href="#">Edit</a></li>
                                          <li role="separator" class="divider"></li>
                                        <li><a href="#">Delete</a></li>
                                      </ul>
                                    </div>
                                  </td>
                                </tr>
                              <?php } ?>
                          </tbody>

                        </table>
                      </div>
                  </div>
                </div>


              </div>
          </div>

        </div>
      </div>
  </div>
</div>
