<div class="wrapper wrapper-content">

  <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h4>Selamat Datang Admin Pejabat Pengadaan</h5>
          <div class="ibox float-e-margins">
              <div class="ibox-title text-center">
                  <h2>Daftar Paket Lelang Yang Aktif</h5>
                </div>
              <div class="ibox-content">
                <div class="row">
                  <div class="table-responsive col-md-12">
                      <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                        <tr>
                            <th>Kode Pengadaan</th>
                            <th>Nama Pengadaan</th>
                            <th>Vendor Pemenang</th>
                            <th>Tanggal(s)</th>
                            <th>Jumlah Peserta</th>

                        </tr>
                        </thead>
                        <tbody>
                          <?php
                            foreach ($daftar_pengadaan as $k) { ?>
                              <tr>
                                <td><?php echo $k->id_pengadaan;?></td>
                                <td><?php echo $k->nama_pengadaan;?></td>
                                <td><?php echo $k->nama_perusahaan;?></td>
                                <td><?php echo $k->jadwal_pengadaan;?></td>
                                <td><?php echo $k->peserta;?></td>

                              </tr>
                            <?php } ?>
                        </tbody>

                      </table>
                    </div>


                </div>

                <div class="row">
                  <div class="col-md-12" style="text-align:center">
                    <button type='' class='btn  btn-success '><i class="fa fa-list"></i> Riwayat Pengadaan</button>

                  </div>
                </div>
              </div>
          </div>

        </div>
      </div>
  </div>
</div>
