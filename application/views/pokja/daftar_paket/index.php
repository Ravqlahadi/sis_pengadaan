<div class="wrapper wrapper-content">

  <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h4>Selamat Datang Admin Pejabat Pengadaan</h5>
          <div class="ibox float-e-margins">
              <div class="ibox-title text-center">
                  <h2>Daftar Paket</h5>
                </div>
              <div class="ibox-content">
                <div class="row">
                  <div class="col-md-12" style="margin-bottom:20px">
                    <div class="row">
                      <div class="col-md-3">
                        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-plus"></i> Buat Pengadaan
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Tambah Pengadaan</h4>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                                  <form class="" action="<?php echo site_url('pokja/input_pengadaan')?>" method="post" enctype="multipart/form-data">
                                      <div class="form-horizontal">
                                        <div class="col-md-12 form-regis">
                                          <label class="col-sm-2 col-md-offset-1 control-label" >Kode Pengadaan</label>
                                            <div class="col-sm-7"><input type="text" class="form-control" name="id_pengadaan"></div>
                                        </div>
                                        <div class="col-md-12 form-regis">
                                          <label class="col-sm-2 col-md-offset-1 control-label" >Nama Pengadaan</label>
                                            <div class="col-sm-7"><input type="text" class="form-control" name="nama_pengadaan"></div>
                                        </div>
                                        <div class="col-md-12 form-regis">
                                          <label class="col-sm-2 col-md-offset-1 control-label" >Jenis Pengadaan</label>
                                            <div class="col-sm-7">
                                              <select class='form-control' name='jenis_pengadaan'>
                                                <option value="pengadaan_barang">Pengadaan Barang</option>
                                                <option value="pengadaan_barang">Pengadaan Jasa</option>
                                              </select>
                                            </div>
                                        </div>

                                        <div class="col-md-12 form-regis">
                                          <label class="col-sm-2 col-md-offset-1 control-label" >Lokasi Pengerjaan</label>
                                            <div class="col-sm-7"><input type="text" class="form-control" name="lokasi_pengadaan"></div>
                                        </div>
                                        <div class="col-md-12 form-regis">
                                          <label class="col-sm-2 col-md-offset-1 control-label" >Jadwal Mulai Pekerjaan</label>
                                            <div class="col-sm-7"><input type="date" class="form-control" name="jadwal_mulai"></div>
                                        </div>
                                        <div class="col-md-12 form-regis">
                                          <label class="col-sm-2 col-md-offset-1 control-label" >Jadwal Selesai Pekerjaan</label>
                                            <div class="col-sm-7"><input type="date" class="form-control" name="jadwal_selesai"></div>
                                        </div>
                                        <div class="col-md-12 form-regis">
                                          <label class="col-sm-2 col-md-offset-1 control-label" >Dokumen</label>
                                            <div class="col-sm-7"><input type="file" class="form-control" name="userfile"></div>
                                        </div>
                                        <div class="col-md-12">
                                          <div class="col-md-8 col-md-offset-2"  style="margin-top:30px;float:right">
                                            <button type='submit' class='btn btn-md btn-success '>Simpan</button>
                                            <button type='submit' class='btn btn-md btn-warning '>Batal</button>
                                          </div>
                                        </div>
                                      </div>
                                  </form>

                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                          <thead>
                          <tr>
                              <th>Kode</th>
                              <th>Nama Pengadaan</th>
                              <th>Jenis Pengadaan</th>
                              <th>Jadwal</th>
                              <th>Lokasi</th>
                              <th>HPS</th>
                              <th>Dokumen</th>
                              <th>Status</th>
                              <th>Aksi</th>
                          </tr>
                          </thead>
                          <tbody>
                            <?php
                              foreach ($daftar_pengadaan as $k) { ?>
                                <tr>
                                  <td><?php echo $k->id_pengadaan;?></td>
                                  <td><?php echo $k->nama_pengadaan;?></td>
                                  <td><?php echo $k->jenis_pengadaan;?></td>
                                  <td><?php echo $k->jadwal_pengadaan;?></td>
                                  <td><?php echo $k->lokasi_pekerjaan;?></td>
                                  <td>Rp.<?php echo $k->nilai_hps;?></td>
                                  <td><a href="<?php echo base_url('upload/dokumen')."/".$k->dokumen?>"> <?php echo $k->dokumen;?></a></td>
                                  <td><?php echo $k->status;?></td>
                                  <td>
                                    <div class="dropdown">
                                      <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        Aksi
                                        <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        <li><a href="<?php echo site_url('pokja/detail_hps').'?id='.$k->id_pengadaan?>">Detail HPS</a></li>
                                        <li><a href="<?php echo site_url('pokja/detail_pengadaan').'?id='.$k->id_pengadaan?>">Detail Pengadaan</a></li>
                                          <li><a href="#">Edit</a></li>
                                          <li role="separator" class="divider"></li>
                                        <li><a href="#">Delete</a></li>
                                      </ul>
                                    </div>
                                  </td>
                                </tr>
                              <?php } ?>
                          </tbody>

                        </table>
                      </div>
                  </div>
                </div>
              </div>
          </div>

        </div>
      </div>
  </div>
</div>
