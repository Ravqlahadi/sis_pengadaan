<div class="wrapper wrapper-content">

  <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h4>Selamat Datang Admin Pejabat Pengadaan</h5>
          <div class="ibox float-e-margins">
              <div class="ibox-title text-center">
                  <h2><?php $k=$data_pengadaan[0];
                  echo $k->nama_pengadaan;
                  ?></h5>
                </div>
              <div class="ibox-content">
                <div class='row '>
                  <div class="tabs-container">
                       <ul class="nav nav-tabs">
                           <li class="active"><a data-toggle="tab" href="#tab-1">Informasi Lelang</a></li>
                           <li class=""><a data-toggle="tab" href="#tab-2">Penjelasan</a></li>
                           <li class=""><a data-toggle="tab" href="#tab-3">Penawaran Harga</a></li>
                       </ul>
                       <div class="tab-content">
                           <div id="tab-1" class="tab-pane active">
                               <div class="panel-body">
                                 <div class="col-md-12 form-regis">
                                   <label class="col-sm-2 col-md-offset-1 control-label" >Kode Pengadaan</label>
                                     <div class="col-sm-7"><input type="text" class="form-control" name="id_pengadaan" readonly value="<?php echo $k->id_pengadaan;?>"></div>
                                 </div>
                                 <div class="col-md-12 form-regis">
                                   <label class="col-sm-2 col-md-offset-1 control-label" >Nama Pengadaan</label>
                                     <div class="col-sm-7"><input type="text" class="form-control" name="nama_pengadaan" readonly value="<?php echo $k->nama_pengadaan;?>"></div>
                                 </div>
                                 <div class="col-md-12 form-regis">
                                   <label class="col-sm-2 col-md-offset-1 control-label" >Jenis Pengadaan</label>

                                     <div class="col-sm-7"><input type="text" class="form-control" name="lokasi_pengadaan" readonly value="<?php echo $k->jenis_pengadaan;?>"></div>

                                 </div>
                                 <div class="col-md-12 form-regis">
                                   <label class="col-sm-2 col-md-offset-1 control-label" >Jenis Pengadaan</label>

                                     <div class="col-sm-7"><input type="text" class="form-control" name="lokasi_pengadaan" readonly value="Rp.<?php echo $k->nilai_hps;?>"></div>

                                 </div>
                                 <div class="col-md-12 form-regis">
                                   <label class="col-sm-2 col-md-offset-1 control-label" >Lokasi Pengerjaan</label>
                                     <div class="col-sm-7"><input type="text" class="form-control" name="lokasi_pengadaan" readonly value="<?php echo $k->lokasi_pekerjaan;?>"></div>
                                 </div>
                                 <div class="col-md-12 form-regis">
                                   <label class="col-sm-2 col-md-offset-1 control-label" >Tanggal Pengadaan</label>
                                     <div class="col-sm-7"><input type="text" class="form-control" name="lokasi_pengadaan" readonly value="<?php echo $k->jadwal_pengadaan?>"></div>
                                 </div>
                                 <div class="col-md-12 form-regis">
                                   <label class="col-sm-2 col-md-offset-1 control-label" >Jumlah Peserta</label>
                                     <div class="col-sm-4"><input type="text" class="form-control" name="jadwal_mulai" readonly value="<?php echo $k->peserta?>"></div>
                                     <div class="col-md-3">
                                       <button type='button' class='btn btn-default'>Detail</button>

                                     </div>
                                 </div>

                                 <div class="col-md-12 form-regis">
                                   <label class="col-sm-2 col-md-offset-1 control-label" >Dokumen</label>
                                     <div class="col-sm-7"><a class="btn btn-warning" target="_blank" href="<?php echo base_url()?>/upload/dokumen/<?php echo $k->dokumen;?>"><i class="fa fa-download"></i> Unduh</a></div>
                                 </div>

                               </div>
                           </div>
                           <div id="tab-2" class="tab-pane">
                               <div class="panel-body">
                                 <div class="panel-body">
                                   <div class="row">
                                     <div class="col-md-12">
                                       <div class="col-md-12 form-regis">
                                         <label class="col-sm-2 col-md-offset-1 control-label" >Kode Pengadaan</label>
                                           <div class="col-sm-7"><input type="text" class="form-control" name="id_pengadaan" readonly value="<?php echo $k->id_pengadaan;?>"></div>
                                       </div>
                                       <div class="col-md-12 form-regis">
                                         <label class="col-sm-2 col-md-offset-1 control-label" >Nama Pengadaan</label>
                                           <div class="col-sm-7"><input type="text" class="form-control" name="nama_pengadaan" readonly value="<?php echo $k->nama_pengadaan;?>"></div>
                                       </div>
                                       <div class="col-md-12 form-regis">
                                         <label class="col-sm-2 col-md-offset-1 control-label" >Status Pengadaan</label>

                                           <div class="col-sm-7"><input type="text" class="form-control" name="lokasi_pengadaan" readonly value="buka"></div>

                                       </div>
                                     </div>
                                   </div>
                                   <div class="row" style="margin-top:30px">
                                     <div class="col-md-12">
                                        <div class="col-md-10 col-md-offset-1">
                                          <table class='table table-striped table-bordered table-hover table-condensed'>
                                            <thead>
                                              <tr>
                                                <th>No</th>
                                                <th>IDMember</th>
                                                <th>Pertanyaan</th>
                                                <th>Jawaban</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                              <tr>
                                                <td>1</td>
                                                <td>MB001</td>
                                                <td>Spefisikasi seperti apa?</td>
                                                <td>
                                                  <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal">
                                                      <i class="fa fa-information"></i> Jawab
                                                  </button>
                                                  <!-- Modal -->
                                                  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                      <div class="modal-content">
                                                        <div class="modal-header">
                                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                          <h4 class="modal-title" id="myModalLabel">Jawab Pertanyaan</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                          <div class="row">
                                                            <form class="" action="<?php echo site_url('pokja/input_hps')?>" method="post" enctype="multipart/form-data">
                                                              <div class="form-horizontal">

                                                                <div class="col-md-12 form-regis">
                                                                  <label class="col-sm-2 col-md-offset-1 control-label" >Uraian Pertanyaan</label>
                                                                    <div class="col-sm-7"><input type="text" id="volBar" class="form-control" name="volume_barang" onchange="tambhVol()">
                                                                      <input type="hidden" id="volBar" class="form-control" name="id_hps" value="Spesifikasi seperti apa?" readonly="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12 form-regis">
                                                                  <label class="col-sm-2 col-md-offset-1 control-label" >Jawaban</label>
                                                                    <div class="col-sm-7"><input type="text" id="volBar" class="form-control" name="volume_barang" onchange="tambhVol()">
                                                                      <input type="hidden" id="volBar" class="form-control" name="id_hps" value="<?php echo $this->input->get('id')?>">
                                                                    </div>
                                                                </div>

                                                              </div>
                                                            </form>
                                                          </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                          <button type="button" class="btn btn-primary">Save changes</button>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>

                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>

                                        </div>
                                      </div>
                                   </div>

                                 </div>
                               </div>
                           </div>
                           <div id="tab-3" class="tab-pane">
                               <div class="panel-body">
                                 <table class="table table-striped table-bordered">
                                    <thead>
                                      <tr>
                                        <th>No</th>
                                        <th>Nama Peserta</th>
                                        <th>Administrasi</th>
                                        <th>Teknis</th>
                                        <th>Harga Penawaran</th>
                                        <th>Pemenang</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <?php
                                      $no = 0;
                                      foreach ($data_penawar as $k) {
                                        ?>
                                        <tr>
                                          <td><?php
                                          $no++;
                                          echo $no;?></td>
                                          <td><?php echo $k->nama_perusahaan;?></td>
                                          <td>
                                            <div class='checkbox-inline'>
                                              <label>
                                                  <input type='checkbox' name='' value='' checked disabled>

                                              </label>
                                            </div>
                                          </td>
                                          <td><div class='checkbox-inline'>
                                            <label>
                                              <input type='checkbox' name='' value='' checked disabled>

                                            </label>
                                          </div></td>
                                          <td><?php echo $k->nilai_penawaran;?></td>
                                          <td><a href="<?php echo site_url('pokja/pemenang');echo "?id_pengadaan=".$k->id_pengadaan."&&id_member=".$k->id_member;?>"><button class='btn btn-success '>Pilih</button></a>
                                          </td>
                                        </tr>

                                      <?php
                                      }?>
                                    </tbody>
                                  </table>
                               </div>
                           </div>

                       </div>


                   </div>

                </div>
              </div>
          </div>

        </div>
      </div>
  </div>
</div>
