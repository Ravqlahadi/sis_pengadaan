<div class="wrapper wrapper-content">

  <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h4>Selamat Datang Admin Pejabat Pengadaan</h5>
          <div class="ibox float-e-margins">
              <div class="ibox-title text-center">
                  <h2>Bukti Pembayaran</h5>
                </div>
              <div class="ibox-content">


                <div class="row" style="margin-top:40px">
                  <div class="col-md-12 form-horizontal">

                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Nomor Surat</label>
                          <div class="col-sm-7"><input type='' class='form-control ' placeholder=''>
                          </div>
                      </div>
                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Tanggal</label>
                          <div class="col-sm-7"><input type='' class='form-control ' placeholder=''>
                          </div>
                      </div>
                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Nama Perusahaan</label>
                          <div class="col-sm-7"><input type='' class='form-control ' placeholder=''>
                          </div>
                      </div>
                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Nama Pengadaan</label>
                          <div class="col-sm-7"><input type='' class='form-control ' placeholder=''>
                          </div>
                      </div>
                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Jenis Pengadaan</label>
                          <div class="col-sm-7"><input type='' class='form-control ' placeholder=''>
                          </div>
                      </div>

                  </div>

                  <div class="col-md-10 col-md-offset-1" style="margin-top:20px;">
                    <h2>Transaksi</h2>
                  </div>

                  <div class="col-md-12 form-horizontal">

                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Nominal</label>
                          <div class="col-sm-7"><input type='' class='form-control ' placeholder=''>
                          </div>
                      </div>
                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Bank</label>
                          <div class="col-sm-7"><input type='' class='form-control ' placeholder=''>
                          </div>
                      </div>
                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label"> Nomor Referensi</label>
                          <div class="col-sm-7"><input type='' class='form-control ' placeholder=''>
                          </div>
                      </div>
                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Rekening Tujuan</label>
                          <div class="col-sm-7"><input type='' class='form-control ' placeholder=''>
                          </div>
                      </div>
                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Nama Penerima</label>
                          <div class="col-sm-7"><input type='' class='form-control ' placeholder=''>
                          </div>
                      </div>
                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Berita</label>
                          <div class="col-sm-7"><textarea name="name" rows="8" cols="80"></textarea>
                          </div>
                      </div>
                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label"></label>
                          <div class="col-sm-7"><button type="button" class="btn btn-success" name="button">Simpan</button>
                          </div>
                      </div>
                  </div>

                </div>


              </div>
          </div>

        </div>
      </div>
  </div>
</div>
