<div class="wrapper wrapper-content">

  <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h4>Selamat Datang Admin Pejabat Pengadaan</h5>
          <div class="ibox float-e-margins">
              <div class="ibox-title text-center">
                  <h2>Daftar Vendor</h5>
                </div>
              <div class="ibox-content">
                <div class="row">
                  <div class="col-md-8 col-md-offset-2">
                    <div class="row">
                      <form class="form-horizontal" action="<?php echo site_url('pokja/daftar_vendor')?>" method="post">
                        <div class="col-md-8">
                          <label class="control-label" ></label>
                            <div class="">
                              <select id="mySelect" class="form-control" name="id_vendor" onchange="myFunction()">
                                <option value="">Pilih Perusahaan</option>
                                  <?php foreach ($daftar_vendor as $k) {

                                    ?>
                                      <option value="<?php echo $k->id_vendor?>"><?php echo $k->nama_perusahaan;?>
                                <?php

                                  }?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                          <button type='submit' class='btn btn-warning' style="margin-top:16px">Pilih</button>

                        </div>
                      </form>
                    </div>
                  </div>
                </div>

                <?PHP if ($data_perusahaan!="") {
                    $data_perusahaan  = $data_perusahaan[0];
                ?>
                <div class="row" style="margin-top:40px">
                  <div class="col-md-12 text-center">
                    <h2><b><?php echo $data_perusahaan->nama_perusahaan;?></b></h2>
                  </div>

                  <div class="col-md-10 col-md-offset-1" style="margin-top:20px;">
                    <h3>A. Data Administrasi</h3>
                  </div>

                  <div class="col-md-12 form-horizontal">

                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Alamat</label>
                          <div class="col-sm-7"><textarea class='form-control' name="alamat" placeholder='' readonly><?php echo $data_perusahaan->alamat;?></textarea>
                          </div>
                      </div>
                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Nomor Telepon</label>
                          <div class="col-sm-7"><input type="text" class="form-control" name="nomor_telepon" readonly value="<?php echo $data_perusahaan->no_telpon?>"> </div>
                      </div>
                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Email</label>
                          <div class="col-sm-7"><input type="text" class="form-control" name="email" readonly value="<?php echo $data_perusahaan->email?>"></div>
                      </div>

                  </div>

                  <div class="col-md-10 col-md-offset-1" style="margin-top:20px;">
                    <h3>B. Izin Usaha</h3>
                  </div>

                  <div class="col-md-12 form-horizontal">

                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Nomor Izin Usaha</label>
                          <div class="col-sm-7"><input type="text" class="form-control" name="nomor_izin_usaha" readonly value="<?php echo $data_perusahaan->no_izin_usaha?>"></div>
                      </div>
                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Izin Usaha</label>
                            <div class="col-sm-7"><input type="text" class="form-control" name="izin_usaha" readonly value="<?php echo $data_perusahaan->izin_usaha?>"></div>
                      </div>

                  </div>

                  <div class="col-md-10 col-md-offset-1" style="margin-top:20px;">
                    <h3>C. Data Keuangan</h3>
                  </div>

                  <div class="col-md-12 form-horizontal">

                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">NPWP</label>
                          <div class="col-sm-7"><input type="text" class="form-control" name="npwp" readonly value="<?php echo $data_perusahaan->npwp?>"></div>
                      </div>
                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Bukti Pajak</label>
                          <div class="col-sm-7"><a class="btn btn-warning" target="_blank" href="<?php echo base_url()?>/upload/bukti_bayar_pajak/<?php echo $data_perusahaan->bukti_bayar_pajak;?>"><i class="fa fa-download"></i> Unduh</a></div>
                      </div>
                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Susunan Kepemilikan Saham</label>
                          <div class="col-sm-7"><a class="btn btn-warning" target="_blank" href="<?php echo base_url()?>/upload/susunan_pemilik/<?php echo $data_perusahaan->susunan_pemilik;?>"><i class="fa fa-download"></i> Unduh</a></div>
                      </div>

                  </div>

                  <div class="col-md-10 col-md-offset-1" style="margin-top:20px;">
                    <h3>D. Landasan Hukum Perusahaan</h3>
                  </div>

                  <div class="col-md-12 form-horizontal">

                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">No. Akta Perusahaan</label>
                          <div class="col-sm-7"><input type="text" class="form-control" name="no_akta_perusahaan" readonly value="<?php echo $data_perusahaan->no_akta_perusahaan?>"></div>
                      </div>
                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Tanggal</label>
                          <div class="col-sm-7"><input type="date" class="form-control" name="tanggal_akta" readonly value="<?php echo $data_perusahaan->tanggal?>"></div>
                      </div>
                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Nama Notaris</label>
                          <div class="col-sm-7"><input type="text" class="form-control" name="nama_notaris" readonly value="<?php echo $data_perusahaan->nama_notaris?>"></div>
                      </div>
                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Pengurus Perusahaan</label>
                          <div class="col-sm-7"><a class="btn btn-warning" target="_blank" href="<?php echo base_url()?>/upload/pengurus_perusahaan/<?php echo $data_perusahaan->pengurus_perusahaan;?>"><i class="fa fa-download"></i> Unduh</a></div>
                      </div>

                  </div>

                  <div class="col-md-10 col-md-offset-1" style="margin-top:20px;">
                    <h3>E. Pengalaman Perusahaan</h3>
                  </div>

                  <div class="col-md-12 form-horizontal">

                      <div class="col-md-12  form-regis">
                        <label class="col-sm-2 col-md-offset-1 control-label">Pengalaman Perusahaan</label>
                          <div class="col-sm-7"><a class="btn btn-warning" target="_blank" href="<?php echo base_url()?>/upload/pengalaman_perusahaan/<?php echo $data_perusahaan->pengalaman_perusahaan;?>"><i class="fa fa-download"></i> Unduh</a></div>
                      </div>

                  </div>
                </div>

                <?php } ?>


              </div>
          </div>

        </div>
      </div>
  </div>
</div>
