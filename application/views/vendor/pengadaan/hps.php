<div class="wrapper wrapper-content">

  <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h4>Selamat Datang Peserta Penyedia</h5>
          <div class="row">

                <div class="col-md-12">
                  <div class="ibox float-e-margins">
                      <div class="ibox-title text-center">
                          <h2>Detail HPS</h5>
                        </div>
                      <div class="ibox-content">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example" >
                                  <thead>
                                  <tr>
                                      <th>no</th>
                                      <th>Nama Barang & Jasa</th>
                                      <th>Satuan</th>
                                      <th>Volume</th>
                                      <th>Harga Satuan</th>
                                      <th>Total Harga</th>
                                      <th>Keterangan</th>
                                  </tr>
                                  </thead>
                                </thead>
                                <tbody>
                                  <?php
                                  $no=1;
                                  $total=0;
                                    foreach ($daftar_hps as $k) { ?>
                                      <tr>
                                        <td><?php echo $no;?></td>
                                        <td><?php echo $k->nama_barang;?></td>
                                        <td><?php echo $k->satuan_barang;?></td>
                                        <td><?php echo $k->volume_barang;?></td>
                                        <td><?php echo $k->harga_barang;?></td>
                                        <td><?php echo $k->harga_barang*$k->volume_barang;?></td>
                                        <td><?php //echo $k->keterangan;?></td>
                                      </tr>
                                    <?php
                                    $total = $total+($k->harga_barang*$k->volume_barang);

                                    $no++;
                                  } ?>
                                </tbody>

                                </table>
                              </div>

                          </div>

                        </div>
                        <div class="row" style="margin-bottom:20px">
                            <div class="col-md-4 col-md-offset-8" style="text-align:right">
                              <div class="row">
                                <div class="col-md-6">
                                  <h3>Jumlah</h3>
                                </div>
                                <div class="col-md-6">
                                  <h3>Rp.<?php echo $total;?></h3>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-6">
                                  <h3>PPN 10%</h3>
                                </div>
                                <div class="col-md-6">
                                  <h3>Rp.<?php $ppn=($total*10)/100;echo $ppn?></h3>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-6">
                                  <h3>Total Nilai</h3>
                                </div>
                                <div class="col-md-6">
                                  <h3>Rp.<?php $total_nilai=$total+$ppn;echo $total_nilai;?></h3>
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12" style="text-align:right">
                            <button  onclick="window.history.go(-1); return false;" type='' class='btn btn-md btn-warning '><i class="fa fa-back"></i>Kembali</button>
                          </div>

                        </div>
                      </div>
                  </div>
            </div>
          </div>

        </div>
        <div class="col-md-12">

        </div>
      </div>
  </div>
</div>
