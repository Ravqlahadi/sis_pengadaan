<div class="wrapper wrapper-content">

  <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h4>Selamat Datang Peserta Penyedia</h5>
          <div class="ibox float-e-margins">
              <div class="ibox-title text-center">
                  <h2><?php $k=$data_pengadaan[0];
                  echo $k->nama_pengadaan;
                  ?></h5>
                </div>
              <div class="ibox-content">
                <div class='row '>
                  <div class="tabs-container">
                       <ul class="nav nav-tabs">
                           <li class="active"><a data-toggle="tab" href="#tab-1">Informasi Lelang</a></li>
                           <li class=""><a data-toggle="tab" href="#tab-2">Penjelasan</a></li>
                           <li class=""><a data-toggle="tab" href="#tab-3">Penawaran Harga</a></li>
                       </ul>
                       <div class="tab-content">
                           <div id="tab-1" class="tab-pane active">
                               <div class="panel-body">
                                 <div class="col-md-12 form-regis">
                                   <label class="col-sm-2 col-md-offset-1 control-label" >Kode Pengadaan</label>
                                     <div class="col-sm-7"><input type="text" class="form-control" name="id_pengadaan" readonly value="<?php echo $k->id_pengadaan;?>"></div>
                                 </div>
                                 <div class="col-md-12 form-regis">
                                   <label class="col-sm-2 col-md-offset-1 control-label" >Nama Pengadaan</label>
                                     <div class="col-sm-7"><input type="text" class="form-control" name="nama_pengadaan" readonly value="<?php echo $k->nama_pengadaan;?>"></div>
                                 </div>
                                 <div class="col-md-12 form-regis">
                                   <label class="col-sm-2 col-md-offset-1 control-label" >Jenis Pengadaan</label>

                                     <div class="col-sm-7"><input type="text" class="form-control" name="lokasi_pengadaan" readonly value="<?php echo $k->jenis_pengadaan;?>"></div>

                                 </div>

                                 <div class="col-md-12 form-regis">
                                   <label class="col-sm-2 col-md-offset-1 control-label" >Lokasi Pengerjaan</label>
                                     <div class="col-sm-7"><input type="text" class="form-control" name="lokasi_pengadaan" readonly value="<?php echo $k->lokasi_pekerjaan;?>"></div>
                                 </div>
                                 <div class="col-md-12 form-regis">
                                   <label class="col-sm-2 col-md-offset-1 control-label" >Jadwal Mulai Pekerjaan</label>
                                     <div class="col-sm-7"><input type="text" class="form-control" name="jadwal_mulai" readonly value="<?php echo $k->jadwal_pengadaan;?>"></div>
                                 </div>

                                 <div class="col-md-12 form-regis">
                                   <label class="col-sm-2 col-md-offset-1 control-label" >Dokumen</label>
                                     <div class="col-sm-7"><a class="btn btn-warning" target="_blank" href="<?php echo base_url()?>/upload/dokumen/<?php echo $k->dokumen;?>"><i class="fa fa-download"></i> Unduh</a></div>
                                 </div>

                               </div>
                           </div>
                           <div id="tab-2" class="tab-pane">
                               <div class="panel-body">
                                 <div class="panel-body">
                                   <div class="col-md-12 form-regis">
                                     <label class="col-sm-2 col-md-offset-1 control-label" >Kode Pengadaan</label>
                                       <div class="col-sm-7"><input type="text" class="form-control" name="id_pengadaan" readonly value="<?php echo $k->id_pengadaan;?>"></div>
                                   </div>
                                   <div class="col-md-12 form-regis">
                                     <label class="col-sm-2 col-md-offset-1 control-label" >Nama Pengadaan</label>
                                       <div class="col-sm-7"><input type="text" class="form-control" name="nama_pengadaan" readonly value="<?php echo $k->nama_pengadaan;?>"></div>
                                   </div>
                                   <div class="col-md-12 form-regis">
                                     <label class="col-sm-2 col-md-offset-1 control-label" >Status Pengadaan</label>

                                       <div class="col-sm-7"><input type="text" class="form-control" name="lokasi_pengadaan" readonly value="buka"></div>

                                   </div>
                                   <div class="col-md-12 form-regis">
                                     <label class="col-sm-2 col-md-offset-1 control-label" >Pertanyaan</label>

                                       <div class="col-sm-7"><textarea class='form-control' placeholder='' ></textarea>
                                       </div>

                                   </div>
                                   <div class="col-md-12 form-regis">
                                     <label class="col-sm-2 col-md-offset-1 control-label" ></label>

                                       <div class="col-sm-7"><button type='submit' class='btn btn-success '>Simpan</button>

                                       </div>

                                   </div>
                                 </div>
                               </div>
                           </div>
                           <div id="tab-3" class="tab-pane">
                             <div class="panel-body">
                                <form class="" action="<?php echo site_url('vendor/penawaran_harga')?>" method="post" enctype="multipart/form-data">


                                   <div class="col-md-12 form-regis">
                                     <label class="col-sm-2 col-md-offset-1 control-label" >Kode Pengadaan</label>
                                       <div class="col-sm-7"><input type="text" class="form-control" name="id_pengadaan" readonly value="<?php echo $k->id_pengadaan;?>"></div>
                                   </div>
                                   <div class="col-md-12 form-regis">
                                     <label class="col-sm-2 col-md-offset-1 control-label" >Nama Pengadaan</label>
                                       <div class="col-sm-7"><input type="text" class="form-control" name="nama_pengadaan" readonly value="<?php echo $k->nama_pengadaan;?>"></div>
                                   </div>

                                   <div class="col-md-12 form-regis">
                                     <label class="col-sm-2 col-md-offset-1 control-label" >Nilai Penawaran</label>
                                       <div class="col-sm-7"><input type="text" class="form-control" name="nilai_penawaran"  value=""></div>
                                   </div>

                                   <div class="col-md-12 form-regis">
                                     <label class="col-sm-2 col-md-offset-1 control-label" >Dokumen Kulifikasi</label>
                                       <div class="col-sm-7"><input type="file" class="form-control" name="userfile"></div>
                                   </div>
                                   <div class="col-md-12 form-regis">
                                     <label class="col-sm-2 col-md-offset-1 control-label" ></label>

                                       <div class="col-sm-7"><button type='submit' class='btn btn-success '>Simpan</button>

                                       </div>

                                   </div>
                               </form>
                             </div>
                           </div>
                       </div>


                   </div>

                </div>
              </div>
          </div>

        </div>
      </div>
  </div>
</div>
