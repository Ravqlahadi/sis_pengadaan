<div class="wrapper wrapper-content">

  <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h4>Selamat Datang Peserta Penyedia</h5>
          <div class="ibox float-e-margins">
              <div class="ibox-title text-center">
                  <h2>Daftar Paket Lelang Yang Aktif</h5>
                </div>
              <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                      <thead>
                      <tr>
                          <th>Kode Pengadaan</th>
                          <th>Nama Pengadaan</th>
                          <th>Tanggal(s)</th>
                          <th>Jenis Pengadaan</th>
                          <th>Nilai HPS</th>
                          <th>Aksi</th>
                      </tr>
                      </thead>
                      <tbody>
                        <?php
                          foreach ($daftar_pengadaan as $k) { ?>
                            <tr>
                              <td><?php echo $k->id_pengadaan;?></td>
                              <td><?php echo $k->nama_pengadaan;?></td>
                              <td><?php echo $k->jadwal_pengadaan;?></td>
                              <td><?php echo $k->jenis_pengadaan;?></td>
                              <td>Rp<?php echo $k->nilai_hps;?></td>
                              <td>
                                <div class="dropdown">
                                  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    Aksi
                                    <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <li><a href="<?php echo site_url('vendor/detail_hps').'?id='.$k->id_pengadaan?>">Detail HPS</a></li>
                                  <li><a href="<?php echo site_url('vendor/detail_pengadaan').'?id='.$k->id_pengadaan?>">Detail Pengadaan</a></li>


                                  <!--  <li><a href="#">Tolak</a></li> -->
                                  </ul>
                                </div>
                              </td>
                            </tr>
                          <?php } ?>
                      </tbody>

                    </table>
                  </div>



              </div>
          </div>

        </div>
      </div>
  </div>
</div>
