<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class vendor extends CI_Controller{

  public function __construct()
  {
    parent::__construct();


    $level=$this->session->userdata('level');
    if($level!='vendor' || $level == ""){
      redirect('auth');
    }
    //Codeigniter : Write Less Do More
  }

  function index()
  {
    redirect('vendor/beranda');
  }

  //navigasi menu

  public function beranda(){
    $this->load->model(array('m_pengadaan'));
    $data['daftar_pengadaan']=$this->m_pengadaan->select_diterima();
    $data['menu']='beranda';
    $this->load->view('vendor/index',$data);
  }
  public function pengadaan(){
    $this->load->model(array('m_pengadaan'));
    $data['daftar_pengadaan']=$this->m_pengadaan->select_diterima();
    $data['menu']='pengadaan';
    $this->load->view('vendor/index',$data);
  }

  public function detail_hps(){
    $this->load->model(array('m_hps','m_pengadaan','m_barang'));
    $id_pengadaan = $this->input->get('id');
    $data_pengadaan = $this->m_pengadaan->get($id_pengadaan);
    foreach ($data_pengadaan as $k) {
      if ($k->id_hps=="") {
        $dat['id_hps']='hps_'.$id_pengadaan;
        $this->m_hps->insert($dat);
        $this->m_pengadaan->update($dat,$id_pengadaan);
        redirect('vendor/detail_hps');
      }else {
        $id_hps=$k->id_hps;
      }

    }
    $data['daftar_barang']=$this->m_barang->select();
    $data['daftar_hps']=$this->m_hps->get_hps_detail($id_hps);

    $data['menu']='hps';
    $this->load->view('vendor/index', $data);
  }

  public function detail_pengadaan(){
    $this->load->model(array('m_hps','m_pengadaan','m_barang'));
    $id_pengadaan = $this->input->get('id');
    $data["data_pengadaan"] = $this->m_pengadaan->get($id_pengadaan);
    $data['menu']='detail_pengadaan';
    $this->load->view('vendor/index', $data);
  }

  public function daftar_vendor(){
    $this->load->model(array('m_vendor'));
    $id_member =$this->session->userdata('id_member');

		$data['data_perusahaan']=$this->m_vendor->get($id_member);

		//var_dump($data['data_perusahaan']);
		$data['daftar_vendor']=$this->m_vendor->select();
    $data['menu']='vendor';
    $this->load->view('vendor/index',$data);
  }

  public function logout() {
    $this->session->unset_userdata('username');
    $this->session->unset_userdata('level');
    session_destroy();
    redirect(site_url());
  }
}
