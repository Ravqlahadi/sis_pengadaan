
<!DOCTYPE html>
<html>
  <?php include "head.php";?>
<body class="gray-bg top-navigation">
    <div id="wrapper">
      <?php include "nav.php";?>
        <div class="wrapper wrapper-content">

            <?php include 'content.php';?>


        </div>
    </div>
    <?php include 'footer.php';?>

</body>

<!-- Mirrored from webapplayers.com/inspinia_admin-v2.5/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 02 May 2016 10:05:48 GMT -->
</html>
