
<!DOCTYPE html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Dinas PU | Kabupaten Konawe Utara</title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg top-navigation">

  <div id="wrapper">
          <div id="" class="gray-bg">
            <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-md-1 col-md-offset-1" style="text-align:right">
              <img src="<?php echo base_url()?>/assets/img/logo-konut.png" style="height:100px;" alt="" />
            </div>
            <div class="col-md-8"  style="text-align:center;margin-top:10px" >
              <h1>Layanan Pengadaan Barang dan Jasa Pemerintah</h1> <h1 style="margin-top:-5px">Dinas Pekerjaan Umum Kabupaten Konawe Utara</h1>
            </div>
            <div class="col-md-1" style="padding-top:10px;text-align:left" >
              <img src="<?php echo base_url()?>/assets/img/logo-pu.png" style="height:100px;" alt="" />
            </div>
        </div>
          <div class="row border-bottom white-bg">
          <nav class="navbar navbar-static-top" role="navigation">
            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                <i class="fa fa-reorder"></i>
            </button>
              <div class="navbar-collapse collapse" id="navbar">
                <div class="row">
                  <div class="col-md-8 col-md-offset-1">
                    <ul class="nav navbar-nav">
                      <li>
                          <a href="<?php echo site_url()?>">
                              <i class="fa fa-home"></i> Beranda
                          </a>
                      </li>
                      <li>
                          <a href="#">
                              <i class="fa fa-newspaper-o"></i> Tentang Kami
                          </a>
                      </li>

                    </ul>
                  </div>
                </div>


              </div>
          </nav>
          </div>
          <div class="wrapper wrapper-content">
              <div class="container">
                <div class="row">
                  <div class="col-lg-10 col-md-offset-1">
                  <div class="ibox float-e-margins">

                      <div class="ibox-content">
                        <div class="row">

                              <div class="middle-box text-center loginscreen animated fadeInDown" style="margin-top:-20px">
                                  <div>
                                      <div class="logo-login">
                                          <img id='logo-login' style="width:40%;height:auto" src="<?php echo base_url()?>assets/img/briefcase.png" alt="" />
                                      </div>
                                      <h3>Selamat datang <?php echo $user?></h3>
                                      <p>Silahkan login untuk melanjutkan
                                          <!--Continually expanded and constantly improved Kendari JobSeeker Admin Them (IN+)-->
                                      </p>

                                      <form class="m-t" role="form" method="post" action="<?php echo site_url('auth/cek_login')?>">
                                          <div class="form-group" style="width:100%">
                                              <input type="text" name='username' class="form-control" placeholder="Username" required="">
                                          </div>
                                          <div class="form-group" style="width:100%">
                                              <input type="password" name='password' class="form-control" placeholder="Password" required="">
                                          </div>
                                          <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                                          <p class="text-muted text-center"><small>Belum terdaftar?</small></p>

                                          <a class="btn btn-sm btn-white btn-block" href="<?php echo site_url('home/register_vendor')?>">Buat Akun</a>
                                      </form>

                                  </div>
                              </div>
                        </div>

                      </div>

                  </div>

                </div>
              </div>
          </div>


          </div>
          </div>



    <!-- Mainly scripts -->
    <script src="<?php echo base_url()?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/inspinia.js"></script>
    <script src="<?php echo base_url()?>assets/js/plugins/pace/pace.min.js"></script>
    <script type="text/javascript">
          $('#myModal').on('shown.bs.modal', function () {
      $('#myInput').focus()
      })
    </script>

</body>



</html>
