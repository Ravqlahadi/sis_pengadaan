-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 13, 2017 at 04:46 PM
-- Server version: 5.6.26
-- PHP Version: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pengadaan`
--

-- --------------------------------------------------------

--
-- Table structure for table `tabel_barang`
--

CREATE TABLE IF NOT EXISTS `tabel_barang` (
  `id_barang` varchar(11) NOT NULL,
  `nama_barang` varchar(200) NOT NULL,
  `harga_barang` int(11) NOT NULL,
  `satuan_barang` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_barang`
--

INSERT INTO `tabel_barang` (`id_barang`, `nama_barang`, `harga_barang`, `satuan_barang`) VALUES
('BJ_004', 'Kertas', 50000, 'Rim'),
('JLN_001', 'Pembuatan Jalan', 55000000, 'Km'),
('JSK_001', 'Jasa Perancangan', 50000000, 'Jasa'),
('KR_001', 'Kursi Kantor', 100000, 'Buah'),
('LPT_001', 'Laptop Asus', 5000000, 'Buah'),
('MJ_001', 'Meja Kantor', 300000, 'Buah'),
('PC_001', 'Komputer', 6000000, 'Buah');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_bp`
--

CREATE TABLE IF NOT EXISTS `tabel_bp` (
  `id_bp` int(11) NOT NULL,
  `id_pokja` int(11) NOT NULL,
  `nama_transaksi` varchar(200) NOT NULL,
  `nominal` int(20) NOT NULL,
  `tanggal` date NOT NULL,
  `bank` varchar(200) NOT NULL,
  `rekening_tujuan` varchar(20) NOT NULL,
  `nama_penerima` varchar(200) NOT NULL,
  `berita` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_bstp`
--

CREATE TABLE IF NOT EXISTS `tabel_bstp` (
  `id_bstp` int(11) NOT NULL,
  `id_vendor` int(11) NOT NULL,
  `id_pkk` int(11) NOT NULL,
  `tanggal_bstp` date NOT NULL,
  `nama_pengadaan` varchar(200) NOT NULL,
  `nama_barang` varchar(200) NOT NULL,
  `volume_barang` int(11) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_detail_hps`
--

CREATE TABLE IF NOT EXISTS `tabel_detail_hps` (
  `id_detail` int(11) NOT NULL,
  `id_hps` varchar(11) NOT NULL,
  `id_barang` varchar(100) NOT NULL,
  `volume_barang` varchar(11) NOT NULL,
  `total_harga` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_detail_hps`
--

INSERT INTO `tabel_detail_hps` (`id_detail`, `id_hps`, `id_barang`, `volume_barang`, `total_harga`) VALUES
(12, 'hps_KD003', 'LPT_001', '5', 25000000),
(13, 'hps_KD001', 'JSK_001', '1', 50000000),
(14, 'hps_KD002', 'JLN_001', '1', 55000000),
(16, 'hps_KD004', 'MJ_001', '20', 6000000),
(17, 'hps_KD004', 'KR_001', '20', 2000000);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_hps`
--

CREATE TABLE IF NOT EXISTS `tabel_hps` (
  `id_hps` varchar(11) NOT NULL,
  `nilai_hps` int(100) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_hps`
--

INSERT INTO `tabel_hps` (`id_hps`, `nilai_hps`, `keterangan`) VALUES
('hps_KD001', 55000000, ''),
('hps_KD002', 60500000, ''),
('hps_KD003', 27500000, ''),
('hps_KD004', 8800000, '');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_member`
--

CREATE TABLE IF NOT EXISTS `tabel_member` (
  `id_member` varchar(20) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_member`
--

INSERT INTO `tabel_member` (`id_member`, `username`, `password`, `email`) VALUES
('1', 'tes', 'tes123', 'tes@gmail.com'),
('MBR-20170117191711', 'technos', '47b23433ff96dcef8722c8aa45d29fde', 'technos-studio@gmail.com'),
('MBR-20170123081013', 'raviq', '29856b7390ea7ba044e88dcd637da753', 'raviq.jackal@gmail.com'),
('MBR-20170124162504', 'indopratama', '43001967307d6af042080126001b6215', 'indopratama@gmail.com'),
('MBR-20170128234844', 'cv-123', 'f779308a9f63fb74d95d63db1bbccbb9', 'mediatama@gmail.com'),
('MBR-20170313153719', 'sejahtera', '36fcb788769306c1198232cd4c9e5b46', 'sejahtera@gmail.com'),
('MBR-20170313160038', 'sejahtera', '36fcb788769306c1198232cd4c9e5b46', 'sejahtera@gmail.com'),
('MBR-20170313160241', 'sejahtera', '36fcb788769306c1198232cd4c9e5b46', 'sejahtera@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_penawaran`
--

CREATE TABLE IF NOT EXISTS `tabel_penawaran` (
  `id_penawaran` int(11) NOT NULL,
  `id_pengadaan` varchar(200) NOT NULL,
  `id_member` varchar(200) NOT NULL,
  `dokumen_kualifikasi` varchar(200) NOT NULL,
  `nilai_penawaran` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_penawaran`
--

INSERT INTO `tabel_penawaran` (`id_penawaran`, `id_pengadaan`, `id_member`, `dokumen_kualifikasi`, `nilai_penawaran`) VALUES
(1, '0', '0', 'dokumen_penawaranKD001MBR-20170124162504.pdf', '50000000'),
(2, 'KD001', 'MBR-20170124162504', 'dokumen_penawaranKD001MBR-20170124162504.pdf', '50000000'),
(3, 'KD002', 'MBR-20170124162504', 'dokumen_penawaranKD002MBR-20170124162504.pdf', '600000000'),
(4, 'KD001', 'MBR-20170117191711', 'dokumen_penawaranKD001MBR-20170117191711.pdf', '45000000'),
(5, 'KD001', 'MBR-20170313153719', 'dokumen_penawaranKD001MBR-20170313153719.pdf', '49000000'),
(6, 'KD002', 'MBR-20170313160241', 'dokumen_penawaranKD002MBR-20170313160241.pdf', '50000000'),
(7, 'KD004', 'MBR-20170313153719', 'dokumen_penawaranKD004MBR-20170313153719.pdf', '80000000'),
(8, 'KD004', 'MBR-20170117191711', 'dokumen_penawaranKD004MBR-20170117191711.pdf', '85000000');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_pengadaan`
--

CREATE TABLE IF NOT EXISTS `tabel_pengadaan` (
  `id_pengadaan` varchar(11) NOT NULL,
  `id_pokja` varchar(11) NOT NULL,
  `id_ppk` varchar(11) NOT NULL,
  `id_pemenang` varchar(200) NOT NULL,
  `id_hps` varchar(11) NOT NULL,
  `nama_pengadaan` text NOT NULL,
  `jenis_pengadaan` text NOT NULL,
  `jadwal_pengadaan` varchar(200) NOT NULL,
  `lokasi_pekerjaan` varchar(200) NOT NULL,
  `dokumen` varchar(200) NOT NULL,
  `jumlah_peserta` int(11) NOT NULL,
  `status` enum('diperiksa','diterima','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_pengadaan`
--

INSERT INTO `tabel_pengadaan` (`id_pengadaan`, `id_pokja`, `id_ppk`, `id_pemenang`, `id_hps`, `nama_pengadaan`, `jenis_pengadaan`, `jadwal_pengadaan`, `lokasi_pekerjaan`, `dokumen`, `jumlah_peserta`, `status`) VALUES
('KD001', 'PKJ001', 'PPK001', 'MBR-20170313153719', 'hps_KD001', 'Desain Normalisasi Sungai Ranowuwue', 'Jasa Konstruksi', 'Mulai 2016-05-30 Sampai Dengan 2016-07-30', 'Sungai Ranowuwue', 'dokumen_Desain_Normalisasi_Sungai_Ranowuwue.docx', 0, 'diterima'),
('KD002', 'PKJ001', 'PPK001', '', 'hps_KD002', 'Pembangunan Jalan Desa Amorome', 'Jasa Konstruksi', 'Mulai 2016-06-07 Sampai Dengan 2016-08-07', 'Desa Amorome', 'dokumen_Pembangunan_Jalan_Desa_Amorome.docx', 0, 'diterima'),
('KD003', 'PKJ001', 'PPK001', '', 'hps_KD003', 'Pengadaan Alat Kantor', 'Barang', 'Mulai 2016-04-27 Sampai Dengan 2016-05-13', 'Kantor Instasi', 'dokumen_Pengadaan_Alat_Kantor.docx', 0, 'diterima'),
('KD004', '', '', 'MBR-20170313153719', 'hps_KD004', 'Pengadaan Kursi Meja', 'pengadaan_barang', 'Mulai 2017-03-31 Sampai Dengan 2017-04-30', 'Dinas PU', 'dokumen_Pengadaan_Kursi_Meja.pdf', 0, 'diterima');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_penjelasan_tender`
--

CREATE TABLE IF NOT EXISTS `tabel_penjelasan_tender` (
  `id_penjelasan` int(11) NOT NULL,
  `id_pengadaan` int(11) NOT NULL,
  `id_pokja` int(11) NOT NULL,
  `id_member` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `pertanyaan` text NOT NULL,
  `jawaban` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_po`
--

CREATE TABLE IF NOT EXISTS `tabel_po` (
  `id_po` int(11) NOT NULL,
  `id_ppk` int(11) NOT NULL,
  `id_vendor` int(11) NOT NULL,
  `tanggal_po` date NOT NULL,
  `nama_barang` text NOT NULL,
  `qty` int(11) NOT NULL,
  `total_harga` varchar(100) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_pokja`
--

CREATE TABLE IF NOT EXISTS `tabel_pokja` (
  `id_pokja` int(11) NOT NULL,
  `nama_pokja` varchar(200) NOT NULL,
  `jabatan_pokja` varchar(200) NOT NULL,
  `alamat_pokja` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_pokja`
--

INSERT INTO `tabel_pokja` (`id_pokja`, `nama_pokja`, `jabatan_pokja`, `alamat_pokja`) VALUES
(1, 'Zulkifli ST', 'Panitia Lelang', 'Jl Kijang no 8');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_ppk`
--

CREATE TABLE IF NOT EXISTS `tabel_ppk` (
  `id_ppk` int(11) NOT NULL,
  `nama_ppk` varchar(200) NOT NULL,
  `jabatan_ppk` varchar(200) NOT NULL,
  `alamat_ppk` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_ppk`
--

INSERT INTO `tabel_ppk` (`id_ppk`, `nama_ppk`, `jabatan_ppk`, `alamat_ppk`) VALUES
(1, 'Indra ST', 'Kabid', 'Jln Panjang');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_sp`
--

CREATE TABLE IF NOT EXISTS `tabel_sp` (
  `id_sp` int(11) NOT NULL,
  `id_ppk` int(11) NOT NULL,
  `id_vendor` int(11) NOT NULL,
  `nama_beranda` varchar(200) NOT NULL,
  `nilai_kontrak` int(20) NOT NULL,
  `nama_pengadaan` text NOT NULL,
  `volume_barang` int(11) NOT NULL,
  `tanggal_sp` date NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_spmk`
--

CREATE TABLE IF NOT EXISTS `tabel_spmk` (
  `id_spmk` int(11) NOT NULL,
  `id_ppk` int(11) NOT NULL,
  `id_vendor` int(11) NOT NULL,
  `nama_pengadaan` varchar(200) NOT NULL,
  `nilai_kontrak` int(11) NOT NULL,
  `tanggal_spmk` date NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_user`
--

CREATE TABLE IF NOT EXISTS `tabel_user` (
  `id_user` int(10) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `nama_user` varchar(100) NOT NULL,
  `level` enum('pokja','ppk','vendor','') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_user`
--

INSERT INTO `tabel_user` (`id_user`, `username`, `password`, `nama_user`, `level`) VALUES
(1, 'pokja', 'd3412b73a5187c7fd989a8ef9803819b', 'pokja', 'pokja'),
(2, 'perusahaan', '21f57628aa8d2ba238f7a6db352195c8', 'perusahaan', 'vendor'),
(3, 'ppk', '3bb3f4dbc050a34d9c401067d396db13', 'ppk', 'ppk'),
(8, 'technos', '47b23433ff96dcef8722c8aa45d29fde', 'technos', 'vendor'),
(9, 'raviq', '29856b7390ea7ba044e88dcd637da753', 'raviq', 'vendor'),
(10, 'indopratama', '43001967307d6af042080126001b6215', 'indopratama', 'vendor'),
(11, 'cv-123', 'f779308a9f63fb74d95d63db1bbccbb9', 'cv-123', 'vendor'),
(14, 'sejahtera', '36fcb788769306c1198232cd4c9e5b46', 'sejahtera', 'vendor');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_vendor`
--

CREATE TABLE IF NOT EXISTS `tabel_vendor` (
  `id_vendor` int(11) NOT NULL,
  `id_member` varchar(30) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `no_telpon` varchar(15) NOT NULL,
  `npwp` varchar(100) NOT NULL,
  `izin_usaha` varchar(200) NOT NULL,
  `bukti_bayar_pajak` varchar(200) NOT NULL,
  `susunan_pemilik` varchar(200) NOT NULL,
  `landasan_hukum_perusahaan` varchar(200) NOT NULL,
  `pengalaman_perusahaan` varchar(200) NOT NULL,
  `pengurus_perusahaan` varchar(200) NOT NULL,
  `kode_pos` int(8) NOT NULL,
  `provinsi` varchar(100) NOT NULL,
  `no_izin_usaha` varchar(50) NOT NULL,
  `no_akta_perusahaan` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `nama_notaris` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_vendor`
--

INSERT INTO `tabel_vendor` (`id_vendor`, `id_member`, `nama_perusahaan`, `alamat`, `email`, `no_telpon`, `npwp`, `izin_usaha`, `bukti_bayar_pajak`, `susunan_pemilik`, `landasan_hukum_perusahaan`, `pengalaman_perusahaan`, `pengurus_perusahaan`, `kode_pos`, `provinsi`, `no_izin_usaha`, `no_akta_perusahaan`, `tanggal`, `nama_notaris`) VALUES
(1, 'MBR-20170117191711', 'Technos Studio', 'Jl. Supu Yusuf', 'technos@gmail.com', '0401-319228', '66546009878897', 'SITU, SIUP, TDP', 'Technos Studio-Bukti Pajak.pdf', 'Technos Studio-8108-yuda-asmara-c-3x4.jpg', 'Technos Studio-NAMA KECAMATAN & DESA bombana.xlsx', 'Technos Studio-Dari 4 percobaan.docx', '', 0, '', '1/IZN/I/2017', '1234567890', '2017-01-18', 'Raviq G. Lahadi'),
(2, 'MBR-20170123081013', 'Raviq Graph', 'Disana', 'raviq.jackal@gmail.com', '91921120412', '01291', 'Pengadaan Barang dan Jasa', '', '', '', '', '', 0, '', '0114210213', '21391241', '2017-11-27', 'sdka;sd'),
(3, 'MBR-20170124162504', 'Indo Pratama', 'Jln Korumba No 32', 'indopratama@gmail.com', '0401-21223', '02.477.789.0-788-252', 'Pengadaan Barang dan Jasa', '', '', '', '', '', 0, '', '400/5828A/338.8.11/2016', '21', '2017-10-29', 'Fadlin Arsin SH'),
(4, 'MBR-20170128234844', 'CV. Media Tama', 'Jln. Jend Soedirman, Kav. 56, Kendari, Sulawesi Tenggara', 'mediatama@gmail.com', '081381260883', '74.780.096.9-811.000', 'Penyedia Jasa Konsultasi', '', '', '', '', '', 0, '', '0739/22-09/PK/VIII/08', '110115113175', '2008-08-09', 'Vicky Soemarno, S.H., M.Hum'),
(7, 'MBR-20170313153719', 'CV. Sejahtera', 'jln. bahagia', 'sejahtera@gmail.com', '08233174456', 'tes1234', 'Pengadaan Barang dan Jasa', '', '', '', '', '', 0, '', 'tes1234', 'sejahtera', '2017-01-11', 'Diana SH');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tabel_barang`
--
ALTER TABLE `tabel_barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `tabel_bp`
--
ALTER TABLE `tabel_bp`
  ADD PRIMARY KEY (`id_bp`);

--
-- Indexes for table `tabel_bstp`
--
ALTER TABLE `tabel_bstp`
  ADD PRIMARY KEY (`id_bstp`);

--
-- Indexes for table `tabel_detail_hps`
--
ALTER TABLE `tabel_detail_hps`
  ADD PRIMARY KEY (`id_detail`);

--
-- Indexes for table `tabel_hps`
--
ALTER TABLE `tabel_hps`
  ADD PRIMARY KEY (`id_hps`);

--
-- Indexes for table `tabel_member`
--
ALTER TABLE `tabel_member`
  ADD PRIMARY KEY (`id_member`);

--
-- Indexes for table `tabel_penawaran`
--
ALTER TABLE `tabel_penawaran`
  ADD PRIMARY KEY (`id_penawaran`);

--
-- Indexes for table `tabel_pengadaan`
--
ALTER TABLE `tabel_pengadaan`
  ADD PRIMARY KEY (`id_pengadaan`);

--
-- Indexes for table `tabel_penjelasan_tender`
--
ALTER TABLE `tabel_penjelasan_tender`
  ADD PRIMARY KEY (`id_penjelasan`);

--
-- Indexes for table `tabel_po`
--
ALTER TABLE `tabel_po`
  ADD PRIMARY KEY (`id_po`);

--
-- Indexes for table `tabel_pokja`
--
ALTER TABLE `tabel_pokja`
  ADD PRIMARY KEY (`id_pokja`);

--
-- Indexes for table `tabel_ppk`
--
ALTER TABLE `tabel_ppk`
  ADD PRIMARY KEY (`id_ppk`);

--
-- Indexes for table `tabel_sp`
--
ALTER TABLE `tabel_sp`
  ADD PRIMARY KEY (`id_sp`);

--
-- Indexes for table `tabel_spmk`
--
ALTER TABLE `tabel_spmk`
  ADD PRIMARY KEY (`id_spmk`);

--
-- Indexes for table `tabel_user`
--
ALTER TABLE `tabel_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tabel_vendor`
--
ALTER TABLE `tabel_vendor`
  ADD PRIMARY KEY (`id_vendor`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tabel_bp`
--
ALTER TABLE `tabel_bp`
  MODIFY `id_bp` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tabel_bstp`
--
ALTER TABLE `tabel_bstp`
  MODIFY `id_bstp` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tabel_detail_hps`
--
ALTER TABLE `tabel_detail_hps`
  MODIFY `id_detail` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tabel_penawaran`
--
ALTER TABLE `tabel_penawaran`
  MODIFY `id_penawaran` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tabel_penjelasan_tender`
--
ALTER TABLE `tabel_penjelasan_tender`
  MODIFY `id_penjelasan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tabel_po`
--
ALTER TABLE `tabel_po`
  MODIFY `id_po` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tabel_pokja`
--
ALTER TABLE `tabel_pokja`
  MODIFY `id_pokja` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tabel_ppk`
--
ALTER TABLE `tabel_ppk`
  MODIFY `id_ppk` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tabel_sp`
--
ALTER TABLE `tabel_sp`
  MODIFY `id_sp` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tabel_spmk`
--
ALTER TABLE `tabel_spmk`
  MODIFY `id_spmk` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tabel_user`
--
ALTER TABLE `tabel_user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tabel_vendor`
--
ALTER TABLE `tabel_vendor`
  MODIFY `id_vendor` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
